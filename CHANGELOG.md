# v4.3.1

## CI

- Add an expiration date(1 month) to artifacts

## Carbon

- Fix bug in calculating GHG Protocol and V5 Reglementary results formats
- For the astronomy part, now correctly deal with pre-2018 GHGI

# v4.3.0

## Carbon

- Add research activities to ghgi boundaries


# v4.2.1

## CI

- Playwright: we now have "End-to-End" tests (tests that involve data in and out the database).
Authenticated access is tested using sqlite3 as database backend.

## Core

- Frontend: fix collection mutation on update. We use `splice` on the inner
array instead of mutating the object using `Object.assign` (which mutated the
object pointed and could mess user code)

## Super administration

- Allows user account modification and deletion from administration interface

# 4.2.0

- Introduce `background jobs` to run long running tasks in the background
  - Design choice: wrap some management commands and allow to run them from the REST API
  - Tasks can thus be run from:
    - the management commands (by the adminisgtrator of the machine)
    - the REST API (by any superuser)
    - the administration area in the frontend (by any superuser)

## Simulators

- Purchases: fix spelling

## Transition

- Table of Actions:
    - Observers can now load more items (was limited to 50 previously)
    - Add some counters (#actions, #laboratories, #actions displayed)
- Filters:
    - Observers can now combine using `AND`/`OR` "administrations", "disciplines" and "tags"


## Core

- Add a management command to change an "Administration" label: `python manage.py administrations mv LABEL_SRC LABEL_DEST`

## Carbon

- Fix `populate_commutes` command according to the new model CommuteSection/Survey models



# 4.1.5

## Simulators

- Purchases:
  - Add a warning explainig why the purchase simulator disappeared
  - Remove corresponding route

## Carbon

- Frontend: fix french typo `lien publique -> lien public`
- Backend: allow superuser to access `views.get_ghgi_consumptions`

## Transition

- Frontend: change "intern" tag icon


# 4.1.4


## Simulators

- Food simulator: fix a bug


## Transition

- Add tags: `recrutement, numérique, stagiaire, cdd, permanent`
- Change left menu to keep only `Data/Les données` menu
