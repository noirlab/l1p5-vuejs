#!/bin/bash

# Create docker image containing l1p5 website

# @author  Fabrice Jammes

set -euxo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)
. $DIR/include.sh
load_conf

CACHE_OPT=""

usage() {
  cat << EOD

Usage: `basename $0` [options]

  Available options:
    -c          rebuild from scratch
    -h          this message

Build l1p5 website container image, from source code.
EOD
}

kind=false

# get the options
while getopts hc c ; do
    case $c in
	    h) usage ; exit 0 ;;
	    c) CACHE_OPT="--no-cache" ;;
	    \?) usage ; exit 2 ;;
    esac
done
shift `expr $OPTIND - 1`

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

docker image build $CACHE_OPT --tag "$IMAGE" "$DIR"
