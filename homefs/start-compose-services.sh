#!/usr/bin/env bash

set -euo pipefail

# this script is a bit redundant with start.sh and homefs/start.sh, but this
# takes a more streamlined approach and allows to be dynamically configured

echo "Waiting for DB: Starting apache + built vue site"
# run apache in the background to allow the next services to start
apache2ctl start #-D FOREGROUND

# TODO: Migrations should not be included in the repo - makemigrations should be
# run on new builds or as needed.
echo "Checking for migrations"
python3 ./manage.py migrate

# verify the 'dist' directory exists - part of vue build
if [ ! -d 'dist' ]; then
    npm run build
fi;

# TODO: This port should be dynamically configured as per conf.sh
echo "Starting django dev server on port 8000"
python3 ./manage.py runserver 0.0.0.0:8000 2>&1 > ./log/django_dev.log &

# TODO: Log aggregation for debugging would be nice
echo "Starting vuejs server"
npm run serve 2> ./log/vue_serve.log
