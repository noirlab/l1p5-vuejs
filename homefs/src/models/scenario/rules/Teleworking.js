/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'

const LANG = {
  fr: {
    title: 'Développer le télétravail',
    level1help: 'Augmenter le nombre minimum de jours télétravaillés par personne en prenant en compte la fréquence hebdomadaire de télétravail.',
    description: 'Le télétravail permet de réduire le nombre de trajets domicile-travail qui représentent <strong>4% du total des émissions de GES de la France</strong> [1]. En 2021, 22 % des salariés ont eu recourt au télétravail [2]. Cette pratique concerne essentiellement les cadres et reste applicable seulement pour un salarié sur deux [2].',
    otherbenefits: 'Selon une étude de l\'ADEME de 2020, 60 % des télétravailleurs considèrent que le télétravail régulier leur permet d\'améliorer leur gestion du stress, leur concentration, l’équilibre entre leur vie professionnelle et leur vie personnelle ainsi que leur productivité [3].',
    limits: 'En abolissant la frontière physique entre lieu de travail et lieu de vie privée, le risque d’empiétement du travail sur la vie privée est augmenté.',
    rebounds: 'Le télétravail peut conduire à de <strong>nouvelles consommations énergétiques</strong> au domicile (chauffage, éclairage, ...), à une augmentation de l\'utilisation de flux de données internet mais aussi à des effets rebonds de plus long terme comme un <strong>éloignement du lieu de domicile</strong> ou à l\'achat de <strong>nouvel équipement numérique</strong> [4].',
    manual: 'Le curseur permet de spécifier le nombre minimum de jours télétravaillés par personne et par semaine pour l’ensemble des personnels du laboratoire. Pour un jour de télétravail, toutes les personnes sans télétravail passent à 1 jour de télétravail. Pour deux jours de télétravail, toutes les personnes sans télétravail ou à 1 jour de télétravail passent à 2 jours de télétravail. Il n’y a pas de configuration avancée.',
    humanTitle: function (level1Human) {
      return `Atteindre ${level1Human} jours de télétravail par semaine et par personne`
    }
  },
  en: {
    title: 'Develop teleworking',
    level1help: 'Increase the minimum number of teleworking days per person taking into account the weekly frequency of telework.',
    description: 'Teleworking reduces the number of commuting trips, which account for <strong>4% of France\'s total GHG emissions</strong> [1]. In 2021, 22% of employees teleworked [2]. This practice mainly concerns executives and is applicable to only one in two employees [2].',
    otherbenefits: 'According to a 2020 study by ADEME, 60% of teleworkers believe that regular teleworking helps improve their stress management, concentration, work-life balance, and productivity [3].',
    limits: 'By eliminating the physical boundary between the workplace and the private life, the risk of work encroaching on personal life is increased.',
    rebounds: 'Teleworking can lead to <strong>new energy consumption</strong> at home (heating, lighting, etc.), increased use of internet data flows, and longer-term rebound effects such as <strong>relocation to a further place of residence</strong> or the purchase of <strong>new digital equipment</strong> [4].',
    manual: 'The slider allows you to specify the minimum number of teleworking days per person per week for all laboratory personnel. For one teleworking day, all non-teleworking individuals switch to 1 teleworking day. For two teleworking days, all non-teleworking individuals or those with 1 teleworking day switch to 2 teleworking days. There are no advanced configurations available.',
    humanTitle: function (level1Human) {
      return `Reach ${level1Human} days of teleworking days per person`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Trajet domicile-travail : développer le co-voiturage et les mobilités douces',
    year: '2019',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/trajet-domicile-travail-developper-co-voiturage-mobilites-douces'
  },
  '[2]': {
    title: 'En 2021, en moyenne chaque semaine, un salarié sur cinq a télétravaillé',
    year: '2022',
    authors: 'Institut national de la statistique et des études économiques',
    link: 'https://www.insee.fr/fr/statistiques/6209490'
  },
  '[3]': {
    title: 'Télétravail (im)mobilité et modes de vie',
    year: '2020',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://librairie.ademe.fr/cadic/315/teletravail-modes-de-vie-2020-rapport-partie_1.pdf'
  },
  '[4]': {
    title: 'Caractérisation des effets rebond induits par le télétravail',
    year: '2020',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://librairie.ademe.fr/mobilite-et-transport/3776-caracterisation-des-effets-rebond-induits-par-le-teletravail.html'
  }
}

export default class Teleworking extends Rule {
  name = 'Teleworking'
  module = Modules.COMMUTES
  lang = LANG
  references = REFERENCES

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get level1Max () {
    return 5
  }

  get level1Unit () {
    return 'days'
  }

  get level1Step () {
    return 0.1
  }

  get ticks () {
    return [0, 1, 2, 3, 4, 5]
  }

  tickFormatter (value) {
    return value
  }

  get graphics () {
    return [
      'CommutesNoCommutingDays'
    ]
  }

  compute (commutes) {
    let NO_WORKING_DAYS = 5
    for (let commute of commutes) {
      let days2Force = this.level1 - (NO_WORKING_DAYS - commute.nWorkingDay)
      if (days2Force > 0) {
        if (commute.nWorkingDay2 > 0 && commute.nWorkingDay2 >= days2Force) {
          commute.nWorkingDay2 = commute.nWorkingDay2 - days2Force
          commute.nWorkingDay = commute.nWorkingDay - days2Force
        } else if (commute.nWorkingDay2 > 0 && commute.nWorkingDay2 < days2Force) {
          let leftValue = days2Force - commute.nWorkingDay2
          commute.nWorkingDay = commute.nWorkingDay - commute.nWorkingDay2 - leftValue
          commute.nWorkingDay2 = 0
        } else {
          commute.nWorkingDay = commute.nWorkingDay - days2Force
        }
      }
    }
    return commutes
  }
}
