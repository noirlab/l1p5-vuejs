/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Travel from '@/models/carbon/Travel.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Remplacer l\'avion',
    all: 'Tous',
    position: 'Statut de l\'agent',
    purpose: 'Motif de la mission',
    level1help: 'Remplacer l’avion par le train pour l’ensemble des déplacements inférieurs à une distance  donnée.',
    level2label: '(statut: <strong>__position__</strong>, motif: <strong>__purpose__</strong>)',
    level2help: 'Selectionner un statut et/ou un motif sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'L’aviation représente <strong>4.3 % de l’empreinte carbone en France</strong> [1] et 5,1 % au niveau mondial [2]. Ce mode de transport est, avec la voiture, le mode le plus émetteur de GES. Ainsi, d’après la Base Carbone de l’Ademe (chiffres de 2018), en tenant compte des effets des traînées de condensation, remplacer un vol court-courrier par le TGV, permet de diviser les émissions de GES d’un déplacement par plus de 100 et un vol moyen-courrier par plus de 70  [2]. <br /> L’aviation est un mode de transport qui reste très inégalitaire : seuls <strong>4% des français prennent l’avion</strong> de façon régulière [3].',
    otherbenefits: 'Le train bénéficie de nombreux avantages par rapport à l’avion, comme la possibilité de <strong>travailler plus confortablement</strong>. De plus, les gares sont généralement situées en centre-ville, donc souvent plus proche <strong>proches de la destination finale</strong> du voyageur. ',
    limits: 'Tous les déplacements ne sont pas possibles en train. Les déplacements en train peuvent être plus onéreux que leur équivalent en avion. La différence pourrait cependant être payée sur les économies réalisées sur d\'autres mesures du scénario (limitation des vols longue distance, des achats,...)',
    manual: 'Le curseur permet de choisir la distance en dessous de laquelle tous les trajets en avion sont remplacés par le train. Comme pour la mesure précédente, la configuration avancée permet d’affiner le périmètre de la mesure (statut professionnel, motif du déplacement).',
    humanTitle: function (level1Human, unit) {
      // FIXME(msimonin): deals with levels 2
      return `Remplacer l'avion par du train pour les trajets inférieurs à ${level1Human} ${unit}`
    }
  },
  en: {
    title: 'Replace plane',
    all: 'All',
    position: 'Position',
    purpose: 'Travel purpose',
    level1help: 'Replace airplane travel with train travel for all trips below a certain distance.',
    level2label: '(position: <strong>__position__</strong>, purpose: <strong>__purpose__</strong>)',
    level2help: 'Select a position and/or purpose on which to filter the application of the mitigation measure.',
    description: 'Aviation accounts for <strong>4.3% of the carbon footprint in France</strong> [1] and 5.1% globally [2]. This mode of transport is, along with cars, the most emitter of greenhouse gases. According to the Ademe Carbon Database (2018 data), taking into account the effects of condensation trails, replacing a short-haul flight with a high-speed train (TGV) can reduce GHG emissions by over 100 times, and a medium-haul flight by over 70 times [2]. <br /> Aviation is a mode of transportation that remains highly unequal: only <strong>4% of French people regularly take flights</strong> [3].',
    otherbenefits: 'Train travel offers numerous advantages compared to airplanes, such as the possibility to <strong>work more comfortably</strong>. Additionally, train stations are usually located in city centers, making them often <strong>closer to the traveler\'s final destination</strong>.',
    limits: 'Not all trips are possible by train. Train travel can be more expensive than its equivalent by plane. However, the difference in cost could be offset by savings achieved through other measures in the scenario (limiting long-distance flights, reducing purchases, etc.).',
    manual: 'The slider allows you to choose the distance below which all airplane trips are replaced by train travel. Similar to the previous measure, the advanced configuration allows for further customisation of the measure scope (professional position, purpose of travel).',
    humanTitle: function (level1Human, unit) {
      // FIXME(msimonin): deals with levels 2
      return `Replace airplane travel with train travel for all trips below ${level1Human} ${unit}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Empreinte carbone française moyenne, comment est-elle calculée ?',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo'
  },
  '[2]': {
    title: 'Les idées reçues sur l\'aviation et le climat',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/analyse-faq-aviation-climat'
  },
  '[3]': {
    title: 'Global inequalities in flying',
    year: '2021',
    authors: 'Possible.',
    link: 'https://static1.squarespace.com/static/5d30896202a18c0001b49180/t/605a0951f9b7543b55bb003b/1616513362894/Elite+Status+Global+inequalities+in+flying.pdf'
  }
}

export default class ReplacePlane extends Rule {
  name = 'ReplacePlane'
  module = Modules.TRAVELS
  lang = LANG
  references = REFERENCES

  level2 = {
    position: {
      values: function (travels) {
        let positions = []
        for (let travel of travels) {
          if (!positions.map(obj => obj.id).includes(travel.status)) {
            positions.push({
              id: travel.status,
              label: travel.status
            })
          }
        }
        return positions
      },
      value: []
    },
    purpose: {
      values: function (travels) {
        let purposes = []
        for (let travel of travels) {
          if (!purposes.map(obj => obj.id).includes(travel.purpose)) {
            purposes.push({
              id: travel.purpose,
              label: travel.purpose
            })
          }
        }
        return purposes
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1Unit () {
    return 'km'
  }

  get level1Max () {
    return 3000
  }

  get level1Step () {
    return 100
  }

  get ticks () {
    return [0, 750, 1500, 2250, 3000]
  }

  get graphics () {
    return [
      'PlanesCumulativeEmissions',
      'PlanesAndTrainsEmissionFactors'
    ]
  }

  tickFormatter (value) {
    return value
  }

  compute (travels) {
    for (let travel of travels) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE && section.distance <= this.level1) {
          if (this.level2.position.value.length > 0 && this.level2.purpose.value.length > 0) {
            if (this.level2.position.value.includes(travel.status) && this.level2.purpose.value.includes(travel.purpose)) {
              section.transportation = Travel.MODE_TRAIN
            }
          } else if (this.level2.position.value.length > 0) {
            if (this.level2.position.value.includes(travel.status)) {
              section.transportation = Travel.MODE_TRAIN
            }
          } else if (this.level2.purpose.value.length > 0) {
            if (this.level2.purpose.value.includes(travel.purpose)) {
              section.transportation = Travel.MODE_TRAIN
            }
          } else {
            section.transportation = Travel.MODE_TRAIN
          }
        }
      }
    }
    return travels
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let travel of this.data) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE && section.distance <= 3000) {
          intensity.add(section.getCarbonIntensity(this.year).multiply(travel.amount))
        }
      }
    }
    return intensity
  }
}
