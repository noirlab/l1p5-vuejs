/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Building from '@/models/carbon/Building.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Système de chauffage',
    all: 'Tous',
    building: 'Nom du bâtiment',
    level1help: 'Changer le système de chauffage des bâtiments occupés par le laboratoire.',
    level2label: '(bâtiment: <strong>__building__</strong>)',
    level2help: 'Selectionner le bâtiment sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'Les bâtiments représente près de <strong>20% de l’empreinte carbone nationale</strong> [1]. Les chauffages sont de grands émetteurs de GES pour ceux reposant sur les combustibles fossiles ou de faibles émetteurs sinon (chauffage électrique/Pompe à chaleur, bio-méthane, bois). Remplacer une chaudière au fuel ou au gaz par un chauffage à faible émission permet de diminuer de façon importante la contribution des bâtiments au BGES.',
    limits: 'Le remplacement d’un mode de chauffage à base de combustible (du biométhane au fioul) par le chauffage électrique (et sa variante de pompe à chaleur) ne peut pas être pris en compte dans le scénario car la fraction d’électricité consacrée au chauffage n’est pas connue dans le bilan des laboratoires. <br /> De même le remplacement par un réseau de chauffage urbain n’est pas proposé car il n’y en a pas forcément de disponible dans la localité du laboratoire et les performances changent d’un réseau à l’autre. Le chauffage au bois peut être émetteur de particules fines polluantes.',
    rebounds: 'La réduction de la production de GES par tout système faiblement émetteur peut inciter à chauffer plus les bâtiments.',
    manual: 'La fenêtre d’information détaillée de la mesure montre pour tous les types de chauffage leur empreinte carbone par kWh de chauffage équivalent. Un deuxième graphe montre pour vos bâtiments l’empreinte selon le type de chauffage. Vous pouvez décider de changer le type de chauffage pour tous les bâtiments ou pour une sélection d’entre eux.',
    humanTitle: function (level1Human, level2Human) {
      return `Changer le système des bâtiments ${level2Human} en ${level1Human}`
    }
  },
  en: {
    title: 'Change the heating system',
    all: 'All',
    building: 'Building name',
    level1help: 'Change the laboratory buildings heating system.',
    level2label: '(building: <strong>__building__</strong>)',
    level2help: 'Select a building on which to filter the application of the mitigation measure.',
    description: 'Buildings account for nearly <strong>20% of the national carbon footprint</strong> [1]. Heating systems are high emitters of GHGs for those based on fossil fuels or low emitters otherwise (electric heating/heat pumps, bio-methane, wood). Replacing an oil or gas boiler with a low-emission heating system can significantly reduce the contribution of buildings to GHG emissions.',
    limits: 'The replacement of a fuel-based heating mode (from biomethane to oil) by electric heating (and its heat pump variant) cannot be taken into account in the scenario because the fraction of electricity used for heating is not known in the laboratory balance. <br /> Similarly, replacement by a district heating network is not proposed as there may not be one available in the locality of the laboratory and the performance varies from one network to another. Wood heating can emit fine particle pollution.',
    rebounds: 'The reduction of GHG production by any low-emitting system can provide an incentive to heat buildings more.',
    manual: 'The detailed information window of the measure shows for all heating types their carbon footprint per kWh of equivalent heating. A second graph shows the footprint for your buildings by heating type. You can decide to change the heating type for all buildings or for a selection of them.',
    humanTitle: function (level1Human, level2Human) {
      return `Change the heating system for ${level2Human} buildings to ${level1Human}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Décarboner le chauffage dans le secteur du bâtiment à l’horizon 2035',
    year: '2021',
    authors: 'ADEME (Agence de la transition écologique), RTE (Réseau de transport d\'électricité)',
    link: 'https://www.ecologie.gouv.fr/etude-ademe-rte-decarboner-chauffage-dans-secteur-du-batiment-lhorizon-2035'
  }
}

export default class ChangeHeating extends Rule {
  name = 'ChangeHeating'
  module = Modules.HEATINGS
  lang = LANG
  references = REFERENCES

  level2 = {
    building: {
      values: function (buildings, otherrules = null) {
        let names = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.building.value)
          }
        }
        for (let building of buildings) {
          if (!names.map(obj => obj.id).includes(building.name)) {
            names.push({
              id: building.name,
              label: building.name,
              isDisabled: used.includes(building.name)
            })
          }
        }
        return names
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1Max () {
    if (Building.includeCurrent(this.data)) {
      return 7
    } else {
      return 6
    }
  }

  get level1Unit () {
    return ''
  }

  get tickslimit () {
    return 3
  }

  get level1Ticks () {
    let values = Building.getHeatingSystems()
    if (Building.includeCurrent(this.data)) {
      values = Building.getHeatingSystemsWithCustom(this.data)
    }
    let ticks = []
    for (let i in values) {
      ticks.push({
        value: parseInt(i),
        display: values[i]
      })
    }
    return ticks
  }

  get level1MaxValue () {
    if (this.data && Building.includeCurrent(this.data)) {
      return Building.getHeatingSystemsWithCustom(this.data).indexOf('current')
    } else {
      let allSystems = Building.getHeatingSystemsFromData(this.data)
      return Building.getHeatingSystems().indexOf(allSystems[0])
    }
  }

  initiateLevel1 () {
    if (!this.level1 && this.data) {
      if (Building.includeCurrent(this.data)) {
        this.level1 = Building.getHeatingSystemsWithCustom(this.data).indexOf('current')
      } else {
        let allSystems = Building.getHeatingSystemsFromData(this.data)
        this.level1 = Building.getHeatingSystems().indexOf(allSystems[0])
      }
    }
  }

  labelFormatter (value) {
    if (this.data && Building.includeCurrent(this.data)) {
      return Building.getHeatingSystemsWithCustom(this.data)[value]
    } else {
      return Building.getHeatingSystems()[value]
    }
  }

  get graphics () {
    return [
      'HeatingEmissionFactors'
    ]
  }

  compute (buildings) {
    let allSystems = Building.getHeatingSystems()
    let allSystemsData = Building.getHeatingSystemsFromData(buildings)
    let valueIndex = allSystems.indexOf(allSystemsData[0])
    if (Building.includeCurrent(buildings)) {
      allSystems = Building.getHeatingSystemsWithCustom(buildings)
      valueIndex = allSystems.indexOf('current')
    }
    if (this.level1 < valueIndex) {
      let newType = allSystems[this.level1]
      let buildingsToModify = buildings
      if (this.level2.building.value.length > 0) {
        buildingsToModify = buildings.filter(obj => this.level2.building.value.includes(obj.name))
      }
      for (let building of buildingsToModify) {
        for (let heating of building.heatings) {
          if (heating.type !== 'electric') {
            heating.type = newType
          }
        }
      }
    }
    return buildings
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let building of this.data) {
      for (let heating of building.heatings) {
        intensity.add(building.getHeatingCarbonIntensity(heating, this.year))
      }
    }
    return intensity
  }

  humanTitle (lang) {
    let level1Human = this.labelFormatter(this.level1)
    let level2Human = this.level2.building.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(level1Human, level2Human)
  }
}
