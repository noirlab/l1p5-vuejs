/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import Food from '@/models/carbon/Food.js'

const LANG = {
  fr: {
    title: 'Changer de régime alimentaire',
    null: 'Aucun',
    diet: 'Régime alimentaire alternatif',
    level1help: 'Remplacer un % de repas avec viande ou poisson par un régime alimentaire alternatif.',
    level2label: '(diet: <strong>__diet__</strong>)',
    level2help: 'Selectionner un régime alimentaire alternatif à la viande et au poisson.',
    description: 'L’alimentation est le <strong>deuxième poste d’émission le plus important</strong> en France derrière le transport et représente <strong>24% de l\'empreinte carbone nationale </strong>[1]. Bien que 8% de la consommation en produits agricoles en France est de la <strong>viande rouge</strong> (78kg par an et par personne), celle-ci représente à elle seule <strong>59% des émissions de l\'alimentation</strong> (933 kg eCO2) et <strong>67% de l\'empreinte au sol</strong> (2584 m2) [2].<br /> D\'après une enquête de l\'ADEME de 2023, une grande homogénéité ressort de la comparaison des émissions de ce poste selon différents critères socio-démographiques, revenu inclus [3].',
    otherbenefits: 'La commission EAT-Lancet a élaboré des objectifs scientifiques mondiaux fondés sur les meilleures données disponibles en matière d\'alimentation saine et de production alimentaire durable. <br />Ce <strong>régime de référence sain</strong> se compose essentiellement de <strong>légumes, de fruits, de céréales complètes, de légumineuses, de noix et d\'huiles non saturées</strong>, d\'une quantité faible à modérée de fruits de mer et de volaille, et d\'une quantité faible ou nulle de viande rouge, de viande transformée, de sucre ajouté, de céréales raffinées et de légumes riches en amidon [4].',
    limits: 'L’introduction du repas végétarien est un <strong>sujet sensible</strong> qui peut créer des appréhensions chez les consommateurs. Ces craintes sont le plus souvent associés à la <strong>méconnaissance du repas végétarien et de sa composition</strong> (qualités nutritionnelles, propriétés culinaires, …) mais aussi aux conséquences sur les filières d’élevage française [5].<br /> Les <strong>difficultés de mise en place peuvent aussi venir de certaines parties prenantes de la restauration collective </strong> pour lesquels les freins peuvent venir des conditions d’approvisionnement (disponibilité de l’offre, identification des fournisseurs, …) mais aussi de la préparation (construction du plan alimentaire, techniques culinaires, diversité des recettes, …) [5].',
    manual: 'Une fraction des repas composés de viande ou de poisson est remplacée par un autre régime alimentaire. Le curseur permet de spécifier le pourcentage de remplacement. Par défaut, le régime alimentaire de remplacement est le végétarisme. La configuration avancée permet de spécifier un autre régime alimentaire. Si l’on veut spécifier différents régimes alimentaires, il faut créer autant de mesures que de régime alimentaire de substitution. L’outil vérifie que la somme totale des fractions substituées ne dépasse pas 100%. Ainsi, si on a déjà remplacé 50% des repas composés de viande ou de poisson par un régime végétarien, il ne sera pas possible d’allouer plus de 50% des repas carnés à un nouveau régime alimentaire.',
    humanTitle: function (level1Human, level2Human) {
      return `Changer ${level1Human} % des repas avec viande ou poisson par un régime ${level2Human}`
    }
  },
  en: {
    title: 'Changing diet',
    null: 'None',
    diet: 'Alternative diet',
    level1help: 'Replace a % of meat or fish meals with an alternative diet.',
    level2label: '(diet: <strong>__diet__</strong>)',
    level2help: 'Select an alternative diet to meat and fish.',
    description: 'Food is the <strong>second largest source of emissions</strong> in France after transport, accounting for <strong>24% of the national carbon footprint</strong> [1]. Although 8% of agricultural consumption in France is <strong>red meat</strong> (78kg per person per year), this alone accounts for <strong>59% of food emissions</strong> (933 kg eCO2) and <strong>67% of the ground footprint</strong> (2,584 m2) [2]. <br /> According to a 2023 ADEME survey, a comparison of emissions from this item according to different socio-demographic criteria, including income, reveals a high degree of homogeneity [3].',
    otherbenefits: 'The EAT-Lancet commission has developed global scientific targets based on the best evidence available for healthy diets and sustainable food production. <br />This <strong>healthy reference diet</strong> largely consists of <strong>vegetables, fruits, whole grains, legumes, nuts, and unsaturated oils</strong>, includes a low to moderate amount of seafood and poultry, and includes no or a low quantity of red meat, processed meat, added sugar, refined grains, and starchy vegetables [4].',
    limits: 'The introduction of a vegetarian meal is a sensitive subject that can create apprehension among consumers. These fears are most often associated with a lack of knowledge about vegetarian meals and their composition (nutritional qualities, culinary properties, etc.), but also about the consequences for the French livestock industry [5].<br />Difficulties in implementation may also come from certain stakeholders in the catering industry, who may be hampered not only by supply conditions (availability of supply, identification of suppliers, etc.) but also by preparation (construction of the food plan, culinary techniques, diversity of recipes, etc.) [5].',
    manual: 'A fraction of meat or fish meals is replaced by another diet. Use the slider to specify the percentage of replacement. By default, the replacement diet is vegetarian. Advanced configuration allows you to specify a different diet. If you want to specify different diets, you need to create as many measures as there are replacement diets. The tool checks that the total sum of substituted fractions does not exceed 100%. For example, if you have already replaced 50% of meat or fish meals with a vegetarian diet, it will not be possible to allocate more than 50% of meat meals to a new diet.',
    humanTitle: function (level1Human, level2Human) {
      return `Change ${level1Human} % of meals with meat or fish on a ${level2Human} diet.`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Empreinte carbone française moyenne, comment est-elle calculée ?',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo'
  },
  '[2]': {
    title: 'Empreinte carbone et sol de l\'alimentation des Français.es',
    year: '2022',
    authors: 'ADEME (Agence de la transition écologique), RTE (Réseau de transport d\'électricité)',
    link: 'https://librairie.ademe.fr/consommer-autrement/6174-empreinte-carbone-et-sol-de-l-alimentation-des-francaises.html'
  },
  '[3]': {
    title: 'Enquête Empreinte carbone auprès d\'un échantillon représentatif de la population française',
    year: '2023',
    authors: 'ADEME (Agence de la transition écologique), RTE (Réseau de transport d\'électricité)',
    link: 'https://librairie.ademe.fr/changement-climatique-et-energie/6486-enquete-empreinte-carbone-aupres-d-un-echantillon-representatif-de-la-population-francaise.html'
  },
  '[4]': {
    title: 'Food in the Anthropocene: The Eat-Lancet Commission on Healthy Diets from Sustainable Food Systems',
    year: '2019',
    authors: 'Willett, W. and al.',
    link: 'https://www.thelancet.com/pdfs/journals/lancet/PIIS0140-6736(18)31788-4.pdf'
  },
  '[5]': {
    title: 'Freins et leviers pour une restauration collective scolaire plus durable',
    year: '2021',
    authors: 'ADEME (Agence de la transition écologique), RTE (Réseau de transport d\'électricité)',
    link: 'https://librairie.ademe.fr/consommer-autrement/4556-freins-et-leviers-pour-une-restauration-collective-scolaire-plus-durable.html'
  }
}

export default class ChangeDiet extends Rule {
  name = 'ChangeDiet'
  module = Modules.FOODS
  lang = LANG
  references = REFERENCES

  level2 = {
    diet: {
      values: function (foods, otherrules = null) {
        let diets = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.diet.value)
          }
        }
        for (let diet of Food.dietsWithoutMeatAndFish) {
          if (!diets.map(obj => obj.id).includes(diet)) {
            diets.push({
              id: diet,
              label: diet,
              isDisabled: used.includes(diet)
            })
          }
        }
        return diets
      },
      unique: true,
      value: ['vegan']
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get level1MaxValue () {
    let otherrules = this.scenario.rules.filter(obj => obj.name === this.name && obj.id !== this.id)
    return 100 - otherrules.map(obj => obj.level1).reduce((a, b) => a + b, 0)
  }

  get graphics () {
    return [
      'FoodsPie',
      'FoodsEmissionFactors',
      'MealsEmissionFactors'
    ]
  }

  compute (foods) {
    for (let food of foods) {
      let rfood = this.data.filter(obj => obj.seqID === food.seqID)[0]
      for (let meal of food.meals) {
        if (!Food.dietsWithoutMeatAndFish.includes(meal.type) && this.level2.diet.value !== null) {
          let rmeal = rfood.meals.filter(obj => obj.type === meal.type)[0]
          let newAmount = rmeal.amount * this.level1 / 100
          meal.amount -= newAmount
          food.addMeal({
            type: this.level2.diet.value[0],
            amount: newAmount
          })
        }
      }
    }
    return foods
  }

  humanTitle (lang) {
    return ''
  }
}
