/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Électrification du parc',
    level1help: 'Électrifier une fraction des voitures thermiques du parc de véhicules utilisées par votre laboratoire ou loués dans le cadre d’un contrat de leasing.',
    description: 'D’après  l’enquête mobilité des personnes 2018-2019, <strong>la voiture</strong> reste en France le <strong>premier mode de transport</strong> en représentant <strong>62,8 % des trajets</strong> [1] et 20.6 % de l’empreinte carbone nationale [2]. <br /> L\'électrification des voitures thermiques utilisées pour les déplacements professionels permet, dans certaines conditions, de bénéficier de la plus faible empreinte carbone de des voitures.',
    otherbenefits: 'Une voiture électrique permet de <strong>réduire les émissions de polluants atmosphériques</strong> autre que le CO2 et utilise l’énergie électrique moins chère que les carburants fossiles.',
    limits: 'L’électrification du parc automobile représente un levier important afin de réduire les émissions carbone des transports en France. Cependant, plusieurs facteurs peuvent <strong>limiter les bénéfices de ces véhicules</strong>, comme par exemple la <strong>taille du véhicule</strong> et donc de la batterie ou encore les <strong>impacts sociaux et environnementaux</strong> (hors carbone) de la fabrication des batteries [3]. <br /> De plus, il peut y avoir un double comptage de l’empreinte de l’électricité consommée si la recharge est réalisée sur le site du laboratoire.',
    rebounds: ' L’Agence européenne pour l’environnement constate certains effets rebonds suite au déploiement de la voiture électrique dans certains pays comme en Suède et en Norvège : <strong>la voiture électrique remplace</strong> certains <strong>trajets à pied ou en transport en commun</strong> [4].',
    manual: 'Si votre BGES ne contient pas de véhicules de service, il n’est pas possible d’ajouter cette mesure. Le curseur permet de fixer le pourcentage de véhicules de service à convertir en véhicules électriques. Si un seul véhicule est déclaré, le curseur n’accepte que les positions 0 et 100%.',
    humanTitle: function (level1Human) {
      return `Électrification de ${level1Human} % des voitures du parc automobile`
    }
  },
  en: {
    title: 'Fleet electrification',
    level1help: 'Electrify a fraction of the thermal cars in your laboratory fleet or leased under a leasing contract.',
    description: 'According to the 2018-2019 personal mobility survey, <strong>cars</strong> remain the <strong>primary mode of transportation</strong> in France, accounting for <strong>62.8% of journeys</strong> [1] and 20.6% of the national carbon footprint [2]. <br /> Electrifying the combustion engine cars used for business travel can, in certain conditions, enable to benefit from the lower carbon footprint of cars.',
    otherbenefits: 'An electric car can help <strong>reduce atmospheric pollutant emissions</strong> other than CO2 and use electric energy that is cheaper than fossil fuels.',
    limits: 'Electrifying the car fleet is an important lever to reduce carbon emissions from transport in France. However, several factors may <strong>limit the benefits of these vehicles</strong>, such as the <strong>size of the vehicle</strong> and hence of the battery, or the <strong>social and environmental impacts</strong> (excluding carbon) of battery manufacturing [3]. <br /> Additionally, there can be double-counting of the electricity consumption footprint if the recharging is done on the laboratory site.',
    rebounds: 'The European Environment Agency notes some rebound effects following the deployment of electric cars in certain countries such as Sweden and Norway: <strong>electric cars replace</strong> some <strong>trips made by walking or using public transportation</strong> [4].',
    manual: 'If your GHGI does not contain any service vehicles, it is not possible to add this measure. The slider allows you to set the percentage of service vehicles to be converted to electric vehicles. If only one vehicle is declared, the slider only accepts the positions 0 and 100%.',
    humanTitle: function (level1Human) {
      return `Electrify ${level1Human} % of the laboratory fleet`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Comment les Français se déplacent-ils en 2019 ? Résultats de l\'enquête mobilité des personnes',
    year: '2020',
    authors: 'Ministère de la Transition écologique et de la Cohésion des territoires',
    link: 'https://www.statistiques.developpement-durable.gouv.fr/comment-les-francais-se-deplacent-ils-en-2019-resultats-de-lenquete-mobilite-des-personnes'
  },
  '[2]': {
    title: 'Empreinte carbone française moyenne, comment est-elle calculée ?',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo'
  },
  '[3]': {
    title: 'Les idées reçues sur la voiture électrique',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/analyse-faq-voiture-electrique'
  },
  '[4]': {
    title: 'Electric vehicles from life cycle and circular economy perspectives',
    year: '2018',
    authors: 'European Environment Agency',
    link: 'https://www.eea.europa.eu/publications/electric-vehicles-from-life-cycle'
  }
}

export default class FleetElectrification extends Rule {
  name = 'FleetElectrification'
  module = Modules.VEHICLES
  lang = LANG
  references = REFERENCES

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get ticks () {
    let nNotElectricCar = 0
    for (let vehicle of this.data) {
      if (vehicle.type === 'car' && vehicle.engine !== 'electric') {
        nNotElectricCar += 1
      }
    }
    let ticks = [0]
    let stepValue = 100 / nNotElectricCar
    for (let step in [...Array(nNotElectricCar).keys()]) {
      ticks.push(Math.ceil((parseInt(step) + 1) * stepValue))
    }
    return ticks
  }

  get level1Step () {
    let nNotElectricCar = 0
    for (let vehicle of this.data) {
      if (vehicle.type === 'car' && vehicle.engine !== 'electric') {
        nNotElectricCar += 1
      }
    }
    return Math.ceil(100 / nNotElectricCar)
  }

  get graphics () {
    return [
      'VehiclesPie',
      'CarsEmissionFactors'
    ]
  }

  compute (vehicles) {
    let nCar = 0
    for (let vehicle of vehicles) {
      if (vehicle.type === 'car' && vehicle.engine !== 'electric') {
        nCar += 1
      }
    }
    let nCarToKeep = parseInt(nCar * this.level1 / 100)
    let nCarElectrified = 0
    if (nCarToKeep > 0) {
      for (let vehicle of vehicles) {
        if (nCarElectrified < nCarToKeep) {
          if (vehicle.type === 'car' && vehicle.engine !== 'electric') {
            vehicle.engine = 'electric'
            nCarElectrified += 1
          }
        }
      }
    }
    return vehicles
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let vehicle of this.data) {
      if (vehicle.type === 'car' && vehicle.engine !== 'electric') {
        intensity.add(vehicle.getCarbonIntensity(this.year))
      }
    }
    return intensity
  }
}
