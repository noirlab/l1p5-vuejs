/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute.js'
import Food from '@/models/carbon/Food'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'
import { ResearchActivity } from '@/models/carbon/ResearchActivity.js'

export default class Modules {
  // Modules definitions
  static get BUILDINGS () { return 'buildings' }
  static get WATER () { return 'water' }
  static get CONSTRUCTION () { return 'construction' }
  static get HEATINGS () { return 'heatings' }
  static get REFRIGERANTS () { return 'refrigerants' }
  static get ELECTRICITY () { return 'electricity' }
  static get PURCHASES () { return 'purchases' }
  static get DEVICES () { return 'devices' }
  static get VEHICLES () { return 'vehicles' }
  static get TRAVELS () { return 'travels' }
  static get COMMUTES () { return 'commutes' }
  static get FOODS () { return 'foods' }
  static get RACTIVITIES () { return 'ractivities' }

  // Modules colors definitions
  static get WATER_COLOR () { return '#931c63' }
  static get CONSTRUCTION_COLOR () { return '#af4284' }
  static get HEATING_COLOR () { return '#c84896' }
  static get REFRIGERANTS_COLOR () { return '#fdc2e5' }
  static get ELECTRICITY_COLOR () { return '#e88bc3' }
  static get VEHICLES_COLOR () { return '#fd8e62' }
  static get TRAVELS_COLOR () { return '#67c3a5' }
  static get COMMUTES_COLOR () { return '#8ea0cc' }
  static get DEVICES_COLOR () { return '#a6d953' }
  static get PURCHASES_COLOR () { return '#ffda2c' }
  static get FOODS_COLOR () { return '#e5c896' }
  static get RACTIVITIES_COLOR () { return '#b4b4b4' }

  // Modules icons definitions
  static get BUILDINGS_ICON () { return 'building' }
  static get WATER_ICON () { return 'tint' }
  static get CONSTRUCTION_ICON () { return 'wrench' }
  static get HEATING_ICON () { return 'thermometer-full' }
  static get ELECTRICITY_ICON () { return 'plug' }
  static get REFRIGERANTS_ICON () { return 'snowflake-o' }
  static get VEHICLES_ICON () { return 'car' }
  static get TRAVELS_ICON () { return 'train' }
  static get COMMUTES_ICON () { return 'bicycle' }
  static get DEVICES_ICON () { return 'desktop' }
  static get PURCHASES_ICON () { return 'eur' }
  static get FOODS_ICON () { return 'cutlery' }
  static get RACTIVITIES_ICON () { return 'hubzilla' }

  static get MODULES () {
    return {
      [Modules.BUILDINGS]: [
        Modules.WATER,
        Modules.CONSTRUCTION,
        Modules.HEATINGS,
        Modules.ELECTRICITY,
        Modules.REFRIGERANTS
      ],
      [Modules.PURCHASES]: [],
      [Modules.DEVICES]: [],
      [Modules.VEHICLES]: [],
      [Modules.TRAVELS]: [],
      [Modules.COMMUTES]: [],
      [Modules.FOODS]: [],
      [Modules.RACTIVITIES]: []
    }
  }

  static get DEFAULT_COLORS () {
    return {
      [Modules.WATER]: Modules.WATER_COLOR,
      [Modules.CONSTRUCTION]: Modules.CONSTRUCTION_COLOR,
      [Modules.HEATINGS]: Modules.HEATING_COLOR,
      [Modules.REFRIGERANTS]: Modules.REFRIGERANTS_COLOR,
      [Modules.ELECTRICITY]: Modules.ELECTRICITY_COLOR,
      [Modules.PURCHASES]: Modules.PURCHASES_COLOR,
      [Modules.DEVICES]: Modules.DEVICES_COLOR,
      [Modules.VEHICLES]: Modules.VEHICLES_COLOR,
      [Modules.TRAVELS]: Modules.TRAVELS_COLOR,
      [Modules.COMMUTES]: Modules.COMMUTES_COLOR,
      [Modules.FOODS]: Modules.FOODS_COLOR,
      [Modules.RACTIVITIES]: Modules.RACTIVITIES_COLOR
    }
  }

  static getClasse (module) {
    return {
      [Modules.BUILDINGS]: Building,
      [Modules.WATER]: Building,
      [Modules.CONSTRUCTION]: Building,
      [Modules.HEATINGS]: Building,
      [Modules.ELECTRICITY]: Building,
      [Modules.REFRIGERANTS]: Building,
      [Modules.PURCHASES]: Purchase,
      [Modules.DEVICES]: ComputerDevice,
      [Modules.VEHICLES]: Vehicle,
      [Modules.TRAVELS]: Travel,
      [Modules.COMMUTES]: Commute,
      [Modules.FOODS]: Food,
      [Modules.RACTIVITIES]: ResearchActivity
    }[module]
  }

  static getIcon (module) {
    return {
      [Modules.BUILDINGS]: Modules.BUILDINGS_ICON,
      [Modules.WATER]: Modules.WATER_ICON,
      [Modules.CONSTRUCTION]: Modules.CONSTRUCTION_ICON,
      [Modules.HEATINGS]: Modules.HEATING_ICON,
      [Modules.REFRIGERANTS]: Modules.REFRIGERANTS_ICON,
      [Modules.ELECTRICITY]: Modules.ELECTRICITY_ICON,
      [Modules.PURCHASES]: Modules.PURCHASES_ICON,
      [Modules.DEVICES]: Modules.DEVICES_ICON,
      [Modules.VEHICLES]: Modules.VEHICLES_ICON,
      [Modules.TRAVELS]: Modules.TRAVELS_ICON,
      [Modules.COMMUTES]: Modules.COMMUTES_ICON,
      [Modules.FOODS]: Modules.FOODS_ICON,
      [Modules.RACTIVITIES]: Modules.RACTIVITIES_ICON
    }[module]
  }

  static getParentModule (module) {
    return {
      [Modules.BUILDINGS]: Modules.BUILDINGS,
      [Modules.WATER]: Modules.BUILDINGS,
      [Modules.CONSTRUCTION]: Modules.BUILDINGS,
      [Modules.HEATINGS]: Modules.BUILDINGS,
      [Modules.REFRIGERANTS]: Modules.BUILDINGS,
      [Modules.ELECTRICITY]: Modules.BUILDINGS,
      [Modules.PURCHASES]: Modules.PURCHASES,
      [Modules.DEVICES]: Modules.DEVICES,
      [Modules.VEHICLES]: Modules.VEHICLES,
      [Modules.TRAVELS]: Modules.TRAVELS,
      [Modules.COMMUTES]: Modules.COMMUTES,
      [Modules.FOODS]: Modules.FOODS,
      [Modules.RACTIVITIES]: Modules.RACTIVITIES
    }[module]
  }

  static getModules (includesSubModules = false, modules = null) {
    let modulesToReturn = []
    if (includesSubModules) {
      if (modules) {
        for (let module of modules) {
          if (Modules.MODULES[module].length > 0) {
            modulesToReturn = modulesToReturn.concat(Modules.MODULES[module])
          } else {
            modulesToReturn.push(module)
          }
        }
      } else {
        for (let module of Object.keys(Modules.MODULES)) {
          if (Modules.MODULES[module].length > 0) {
            modulesToReturn = modulesToReturn.concat(Modules.MODULES[module])
          } else {
            modulesToReturn.push(module)
          }
        }
      }
    } else {
      if (modules) {
        modulesToReturn = modules
      } else {
        modulesToReturn = Object.keys(Modules.MODULES)
      }
    }
    return modulesToReturn
  }
}
