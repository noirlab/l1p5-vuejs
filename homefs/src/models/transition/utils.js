export function buildDisciplinesTree (disciplines) {
  let categories = [
    {
      label: 'Sciences et technologies (ST)',
      prefix: 'ST',
      id: 'ST',
      children: [
        {
          label: '(ST1) Mathématiques',
          prefix: 'ST1',
          id: 'ST1',
          children: []
        },
        {
          label: '(ST2) Physique',
          prefix: 'ST2',
          id: 'ST2',
          children: []
        },
        {
          label: '(ST3) Sciences de la terre et de l’univers',
          prefix: 'ST3',
          id: 'ST3',
          children: []
        },
        {
          label: '(ST4) Chimie',
          prefix: 'ST4',
          id: 'ST4',
          children: []
        },
        {
          label: '(ST5) Sciences pour l’ingénieur',
          prefix: 'ST5',
          id: 'ST5',
          children: []
        },
        {
          label:
            '(ST6) sciences et technologies de l’information et de la communication',
          prefix: 'ST6',
          id: 'ST6',
          children: []
        }
      ]
    },
    {
      label: 'Sciences du vivant et environnement (SVE)',
      prefix: 'SVE',
      id: 'SVE',
      children: [
        {
          label:
            '(SVE1) Agronomie, Biologie végétale, Écologie, Environnement, Évolution',
          prefix: 'SVE1',
          id: 'SVE1',
          children: []
        },
        {
          label:
            '(SVE2) Biologie cellulaire, Imagerie, Biologie moléculaire, Biochimie, Génomique, Biologie systémique, Développement, Biologie structurale',
          prefix: 'SVE2',
          id: 'SVE2',
          children: []
        },
        {
          label: '(SVE3) Microbiologie, virologie, immunité',
          prefix: 'SVE3',
          id: 'SVE3',
          children: []
        },
        {
          label: '(SVE4) Neurosciences',
          prefix: 'SVE4',
          id: 'SVE4',
          children: []
        },
        {
          label:
            '(SVE5) Physiologie, Physiopathologie, Cardiologie, Pharmacologie, Endocrinologie, Cancer, Technologies médicales',
          prefix: 'SVE5',
          id: 'SVE5',
          children: []
        },
        {
          label: '(SVE6) Santé publique, Épidémiologie, Recherche clinique',
          prefix: 'SVE6',
          id: 'SVE6',
          children: []
        }
      ]
    },
    {
      label: 'Sciences humaines et sociales (SHS)',
      prefix: 'SHS',
      id: 'SHS',
      children: [
        {
          label: '(SHS1) Marchés et organisations',
          prefix: 'SHS1',
          id: 'SHS1',
          children: []
        },
        {
          label: '(SHS2) Normes, institutions et comportements sociaux',
          prefix: 'SHS2',
          id: 'SHS2',
          children: []
        },
        {
          label: '(SHS3) Espaces, environnement et sociétés',
          prefix: 'SHS3',
          id: 'SHS3',
          children: []
        },
        {
          label: '(SHS4) Esprit humain, langage, éducation',
          prefix: 'SHS4',
          id: 'SHS4',
          children: []
        },
        {
          label: '(SHS5) Langues, textes, arts et cultures',
          prefix: 'SHS5',
          id: 'SHS5',
          children: []
        },
        {
          label: '(SHS6) Mondes anciens et contemporains',
          prefix: 'SHS6',
          id: 'SHS6',
          children: []
        }
      ]
    },
    {
      label: 'Services support (SS)',
      prefix: 'SS',
      id: 'SS',
      children: [
        {
          label: '(SS1) Services support',
          prefix: 'SS1',
          id: 'SS1',
          children: []
        }
      ]
    }
  ]

  for (let d of disciplines) {
    for (let cat of categories) {
      if (d.name.startsWith(cat.prefix)) {
        // go down one level
        for (let subCat of cat.children) {
          if (d.name.startsWith(subCat.prefix)) {
            subCat.children.push({ label: d.name, id: d.id })
          }
        }
      }
    }
  }
  return categories
}
