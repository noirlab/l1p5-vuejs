import Travel from '@/models/carbon/Travel.js'
import TravelSection from '@/models/carbon/TravelSection.js'
import Building from '@/models/carbon/Building.js'
import Vehicle from '@/models/carbon/Vehicle.js'
import ComputerDevice from '@/models/carbon/ComputerDevice.js'
import Purchase from '@/models/carbon/Purchase.js'
import Commute from '@/models/carbon/Commute.js'
import Food from '@/models/carbon/Food.js'
import { researchActivityFactory } from '@/models/carbon/ResearchActivity.js'

/**
 * NotImplemented error class for Abstract methods
 */
class NotImplemented extends Error {
  constructor (message = '', ...args) {
    super(message, ...args)
  }
}

/**
 * Manage a list of items
 * We keep an index up-to-date for efficient lookup
 * items are indexed by the result of a given hash value (user provided)
 *
 * Collection aren't supposed to be used directly, subclass it and implement important method instead:
 * - hash
 * - areEquals
 *
 */
export class Collection {
  constructor () {
    // This will store the mapping hash(t) -> index
    this.indexes = new Map()
    // This will store the actual objects
    this.items = []
  }

  /**
   * Get the number of objects
   * @returns the length of the collection
   */
  get length () {
    return this.items.length
  }

  /**
   * Hash function. Has to be implemented by sub-classes.
   * Returns string that considere two items are the same.
   * @param {*} item
   * @returns a string
   */
  hash (item) {
    throw new NotImplemented('Abstract method hash has not been implemented')
  }

  /**
   * Count the number of item returned by the testFunction
   * @param {*} testFunction
   * @param {*} param
   * @returns the number of items that makes the testFunction true
   */
  countItems (testFunction, param) {
    let nb = 0
    for (let item of this.items) {
      if (testFunction(item)) {
        nb++
      }
    }
    return nb
  }

  sortItems () {
    // pass
  }

  /**
   * Assuming the item is already in the list of items
   * make sure to have an index for it.
   *
   * @param {*} item
   */
  _addToIndex (item, index) {
    let h = this.hash(item)
    let indexes = this.indexes.get(h)
    if (indexes === undefined) {
      this.indexes.set(h, [])
      indexes = this.indexes.get(h)
    }
    indexes.push(index)
    // make explicit what we're doing
    // the above already does the job
    this.indexes.set(h, indexes)
  }

  /**
   * Forget about the index for item
   *
   * @param {*} item
   * @param {*} index
   */
  _removeFromIndex (item, index) {
    let h = this.hash(item)
    let indexes = this.indexes.get(h)

    if (indexes === undefined) {
      return
    }

    let iindex = indexes.indexOf(index)
    if (iindex >= 0) {
      indexes.splice(iindex, 1)
    }

    if (indexes.length === 0) {
      this.indexes.delete(h)
    }
  }

  /**
   * Add an item to the collection
   * The hash function is used to know whether we already have the travel recorded.
   * @param {*} item
   */
  add (item) {
    this.items.push(item)
    this._addToIndex(item, this.items.length - 1)
  }

  /**
   * Base function for comparing items
   * two object are considered equals
   * - if they are the same (===)
   * - if they have the same hash
   * (here we assume that by default the hash function is precise enough)
   *
   * beware that this is not the case for Purchases (and must be overriden in this class)
   *
   * @param {*} item1
   * @param {*} item2
   * @returns true iff item1 and item2 are deemed equal
   */
  areEquals (item1, item2) {
    return item1 === item2 || this.hash(item1) === this.hash(item2)
  }

  /**
   * The default update function that keeps reactive property set by vuejs intact.
   * @param {*} index
   * @param {*} ob
   */
  updateFnc (index, obj) {
    this.items.splice(index, 1, obj)
    // Object.assign(this.items[index], obj)
  }

  /**
   * Update an item of the collection.
   * This updates the first object that matches the passed item.
   * and update the index accordingly.
   *
   * @param {*} iobj an object with item, to fields
   */
  update (iobj) {
    let index = this.lookupIndex(iobj.from)
    if (index !== null) {
      // replace the item with the updated version
      // we use assign to keep the

      // beware this works in place so obj.from will be changed !
      this.updateFnc(index, iobj.to)
      // update the index with the new target

      this._removeFromIndex(iobj.from, index)
      this._addToIndex(iobj.to, index)
    }
  }

  /**
   * Reduce the item lists
   */
  reduceItems () {
    return this.items
  }

  /**
   * Some operations require to re-create the index.
   * For instance when we delete a travel the index will get out-of-sync
   * (because posterior index needs to be decremented by 1)
   *
   * O(n) complexity
   */
  reIndex () {
    this.indexes = new Map()
    for (let index in this.items) {
      this._addToIndex(this.items[index], index)
    }
  }

  /**
   * Delete all element which makes cb true.
   *
   * @param {*} cb
   */
  deleteBy (cb) {
    for (let index in this.items) {
      let item = this.items[index]
      if (cb(item)) {
        // set the current item to null
        this.items[index] = null
      }
    }

    // filter the non null items
    this.items = this.items.filter((item) => item !== null)

    this.reIndex()
  }

  /**
   * Delete one item.
   * The first equals to the passed item
   *
   * Complexity: O(n) worst case(when the element is found)
   * @param {*} item
   */
  delete (item) {
    let index = this.lookupIndex(item)
    // remove from index
    if (index != null) {
      this.items.splice(index, 1)
      this._removeFromIndex(item, index)
      this.reIndex()
    }
  }

  reset () {
    this.indexes = new Map()
    this.items = []
  }

  /**
   * Find the first index whose element matches obj
   * null if none can be found.
   *
   * Complexity: O(1)
   *
   * @param {*} obj
   * @returns the index where to find obj in the items (or an equal item)
   */
  lookupIndex (obj) {
    let indexes = this.indexes.get(this.hash(obj))
    if (indexes === undefined) {
      return null
    }
    for (let index of indexes) {
      let item = this.items[index]
      if (this.areEquals(item, obj)) {
        return index
      }
    }
  }

  /**
   *
   * Find if an item already exists into the collection
   *
   * O(1) complexity.
   * @param {*} item
   * @returns the item or null if it can't be found
   */
  lookup (item) {
    let index = this.lookupIndex(item)
    // beware that 0 is a valid index
    if (index !== null) {
      return this.items[index]
    }
    return null
  }

  /**
   * Allows to iterate natively on a collection
   */
  [Symbol.iterator] () {
    return this.items.values()
  }

  /**
   * Use it where vue and our legacy code expect an array
   * @returns an array of items
   */
  toArray () {
    return this.items
  }

  // The following methods return an array
  // This allows to be compatible with the legacy implementation that uses array
  // as collections
  // Unfortunately this isn't really natural as one could expects
  // Collection.filter to return a Collection
  // FIXME: returns a collections (beware that the client code need probably
  // some change)
  filter (cb) {
    return this.items.filter(cb)
  }

  find (cb) {
    return this.items.find(cb)
  }

  map (cb) {
    return this.items.map(cb)
  }
}

export class ModuleCollection extends Collection {
  /**
   * Delete all the items for a given source
   *
   * FIXME: this assumes elements have a source attribute
   *
   *
   * @param {*} source
   */
  deleteSource (source) {
    this.deleteBy((t) => t.source === source)
  }
  /**
   * Delete all items that are invalid
   *
   * FIXME: this assumes elements have an isInvalid method
   *
   * Complexity: O(n)
   *
   */
  deleteInvalid () {
    this.deleteBy((t) => t.isInvalid())
  }
}

/**
 * Manage a list of building
 */
export class BuildingCollection extends ModuleCollection {
  /**
   * Hash function.
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.name
    }
  }

  add (item) {
    let nitem = Building.createFromObj(item)
    super.add(nitem)
  }

  update (iobj) {
    let to = Building.createFromObj(iobj.to)
    super.update({ from: iobj.from, to: to })
  }

  deleteSource (source) {
    // Delete buildings imported from source
    this.deleteBy((t) => t.source === source)
    // Delete refrigerants imported from source
    for (let item of this.items) {
      if (item.sourceRefrigerant === source) {
        item.refrigerants = []
      }
    }
  }
}

/**
 * Manage a list of device
 */
export class DeviceCollection extends ModuleCollection {
  /**
   * Hash function. Two devices with the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.ecodiagObject) {
      return item.ecodiagObject.id
    } else {
      return item.id
    }
  }

  add (item) {
    let nitem
    // Check if item is an ecodiag object (e.g. has a field 'details').
    // if so use fromEcodiag() otherwise create a ComputerDevice object
    // with an empty ecodiagObject field
    if (item.details) {
      nitem = ComputerDevice.fromEcodiag(item)
    } else {
      nitem = ComputerDevice.createFromObj(item)
    }
    super.add(nitem)
  }

  /**
   * Device update handle add and update due to ecodiag
   * FIXME: this breaks the update semantic, why ? item isnot a iobj={from, to}
   * @param {*} item
   * @returns
   */
  update (item) {
    let cd = ComputerDevice.fromEcodiag(item)
    if (this.lookup(item) === null) {
      this.add(item)
    } else {
      // FIXME: Smells here, we assume that the hash doesn't change between the
      // old and the new item
      super.update({ from: item, to: cd })
    }
  }
}

/**
 * Manage a list of commute
 */
export class CommuteCollection extends ModuleCollection {
  /**
   * Hash function. Two commutes with the same seqID
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    return item.seqID
  }

  add (item) {
    let nitem = Commute.createFromObj(item)
    super.add(nitem)
  }

  update (iobj) {
    let to = Commute.createFromObj(iobj.to)
    super.update({ from: iobj.from, to: to })
  }
}

/**
 * Manage a list of food
 */
export class FoodCollection extends ModuleCollection {
  /**
   * Hash function. Two foods with the same seqID
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    return item.seqID
  }

  add (item) {
    let nitem = Food.createFromObj(item)
    super.add(nitem)
  }

  update (iobj) {
    let to = Food.createFromObj(iobj.to)
    super.update({ from: iobj.from, to: to })
  }
}

/**
 * Manage a list of research activities
 */
export class ResearchActivityCollection extends Collection {
  /**
   * Hash function. Two research activities with the same code are considered the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.uuid
    }
  }

  add (item) {
    if ('items' in item && item.items.length) {
      item.items.forEach((el) => {
        let nitem = researchActivityFactory(el)
        super.add(nitem)
      })
    } else {
      let nitem = researchActivityFactory(item)
      super.add(nitem)
    }
  }

  delete (item) {
    if ('items' in item && item.items.length) {
      item.items.forEach((el) => {
        super.delete(el)
      })
    } else {
      super.delete(item)
    }
  }

  update (iobj) {
    if (iobj.from.items.length || iobj.to.items.length) {
      this.delete(iobj.from)
      this.add(iobj.to)
    } else {
      let to = researchActivityFactory(iobj.to)
      super.update({ from: iobj.from, to: to })
    }
  }
}

/**
 * Manage a list of purchases
 */
export class PurchaseCollection extends ModuleCollection {
  /**
   * Hash function. Two purchase with the same code.
   * Beware this will collide on puchases with the same code
   * (this is a feature)
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.code
    }
  }

  /**
   * Override default areEquals function
   * to make sure we're selecting a purchase with the exact same code, amount ...
   * @param {*} item1
   * @param {*} item2
   * @returns
   */
  areEquals (item1, item2) {
    return item1.isEqualTo(item2)
  }

  add (item) {
    let nitem = Purchase.createFromObj(item)
    super.add(nitem)
  }

  /**
   * Update a purchase
   *
   * Known issue:
   * if areEquals(p1, p2) and p1!=p2 (p2 is a copy a p1) then p1 will be
   * modified to p2' (not p2) since we lookup for the first match.  This is
   * fine bacause the intent is respected but might be cumbersome in the UI
   * (this shouldn't happen as long as we're using the original ref to update an
   * object not a copy)
   * @param {*} iobj
   */
  update (iobj) {
    let to = Purchase.createFromObj(iobj.to)
    super.update({ from: iobj.from, to: to })
  }

  reduceItems () {
    return Purchase.reduce(this.items)
  }
}

/**
 * Manage a list of vehicles
 */
export class VehicleCollection extends ModuleCollection {
  /**
   * Hash function. Two vehicles with the same id or name are considered the same
   *
   * @param {*} item
   * @returns
   */
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.name
    }
  }

  add (item) {
    let nitem = Vehicle.createFromObj(item)
    super.add(nitem)
  }

  update (iobj) {
    let to = Vehicle.createFromObj(iobj.to)
    super.update({ from: iobj.from, to: to })
  }
}

/**
 * Manage a list of travels
 *
 * We keep an index up-to-date for efficient lookup
 *
 */
export class TravelCollection extends ModuleCollection {
  /**
   * Hash function. Two travels with the same hash are considered as the same
   * travel
   *
   * @param {*} travel
   * @returns
   */
  hash (travel) {
    return travel.names.join('-') + '_' + travel.amount
  }

  /**
   * Add a travel to the collection
   *
   * - travel correspond to one line in a uploaded file
   * in this case we lookup if it correspond to a previously added one.
   * in this case only the section are added to the lookep-up travel
   * - travel might correspond to full travel (with all the sections) coming
   * from the user form. In this case we also look up before.
   *
   * The hash function is used to know whether we already have the travel recorded.
   *
   * @param {*} travel
   */
  add (travel, needSort = false) {
    let t = this.lookup(travel)
    if (!t) {
      // travel isn't in the collection let's add one
      t = new Travel(
        travel.names,
        travel.date,
        [],
        travel.amount,
        travel.purpose,
        travel.status,
        travel.source
      )
      super.add(t)
    }

    for (let section of travel.sections) {
      t.addSection(TravelSection.createFromObj(section))
    }

    if (needSort && t.sections.length > 0) {
      t.sortSections()
    }
  }

  /**
   * Sort the section for all travels in the collection
   */
  sortItems () {
    for (let t of this.items) {
      t.sortSections()
    }
  }

  countItems (testFunction, param = false) {
    let nb = 0
    if (param) {
      for (let travel of this.items) {
        if (testFunction(travel)) {
          nb += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of this.items) {
        if (testFunction(travel)) {
          nb++
        }
      }
    }
    return nb
  }

  reduceItems () {
    return Travel.reduce(this.items)
  }
}
