/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

/** Represent the carbon intensity of an activity (travels, building). */
export class CarbonIntensity {
  /**
   *
   * Create a new CarbonIntensity
   *
   * @param {number} intensity intensity of carbon in kgCO2eq
   * @param {number} uncertainty uncertainty of the intensity in kgCO2eq
   * @param {number=} group emission group (e.g fossil.fuels for buildings)
   *
   */
  constructor (
    intensity = 0.0,
    uncertainty = 0.0,
    group = null
  ) {
    this.intensity = intensity
    this.uncertainty = uncertainty
    this.group = group
  }

  /**
   *
   * Mutiply the intensity by a factor. Might be used for normalization
   * purpose.
   *
   * @param {number} factor
   *
   */
  multiply (factor) {
    return new CarbonIntensity(
      this.intensity * factor,
      this.uncertainty * factor,
      this.group
    )
  }

  /**
   * Create from another object (typically from the database)
   *
   * @param {*} obj
   */
  static createFromObj (obj) {
    if (obj) {
      return new CarbonIntensity(obj.intensity, obj.uncertainty)
    } else {
      return new CarbonIntensity(0, 0)
    }
  }
}

/**
 * Represent the detailed intensity of an activity (a travel, a building).
 * This means a break down by type of ghg emissions
 */

export class DetailedCarbonIntensity {
  /**
   * @param {number} co2 emissions in kgCO2eq
   * @param {number} ch4 emissions in kgCO2eq
   * @param {number} n2o emissions in kgCO2eq
   * @param {number} other GHG in kgCO2eq
   * @param {number} combustioni part of the carbon intensity due to combustion in kgCO2eq
   * @param {number} combustionu uncertainty on emissions due to combustion in kgCO2eq
   * @param {number} upstreami part of the carbon intensity due to upstream (e.g intensity of petrol extraction) in kgCO2eq
   * @param {number} upstreamu upstream uncertainty
   * @param {number} manufacturingi manufacturing intensity in kgCO2eq
   * @param {number} manufacturingu manufacturing uncertainty
   * @param {number} group emission group (e.g fossil.fuels for buildings)
   *
   */
  constructor (
    co2 = 0.0,
    ch4 = 0.0,
    n2o = 0.0,
    other = 0.0,
    combustioni = 0.0,
    combustionu = 0.0,
    upstreami = 0.0,
    upstreamu = 0.0,
    manufacturingi = 0.0,
    manufacturingu = 0.0,
    group = null
  ) {
    this.co2 = co2
    this.ch4 = ch4
    this.n2o = n2o
    this.other = other
    this.combustion = new CarbonIntensity(combustioni, combustionu)
    this.upstream = new CarbonIntensity(upstreami, upstreamu)
    this.manufacturing = new CarbonIntensity(manufacturingi, manufacturingu)
    this.group = group
  }

  /**
   * Intensity
   * @type {number}
   * @readonly
   */
  get intensity () {
    return this.combustion.intensity + this.upstream.intensity + this.manufacturing.intensity
  }

  /**
   * Uncertainty
   * @type {number}
   * @readonly
   */
  get uncertainty () {
    return this.combustion.uncertainty + this.upstream.uncertainty + this.manufacturing.uncertainty
  }

  /**
   * Make it a CarbonIntensity by aggregating combustion, upstream and manufacturing
   *
   * @returns {CarbonIntensity} a carbon intensity
   */
  toCarbonIntensity () {
    return new CarbonIntensity(
      this.combustion.intensity + this.upstream.intensity + this.manufacturing.intensity,
      this.combustion.uncertainty + this.upstream.uncertainty + this.manufacturing.uncertainty,
      this.group
    )
  }
}

/** Collection of CarbonIntensity.
 *
 */
export class CarbonIntensities {
  /**
   * Create and initialize the collection.
   */
  constructor () {
    this._intensities = new CarbonIntensity()
    this._intensitiesPerMeta = {}
  }

  /**
   *
   *
   * @private
   * @param {CarbonIntensity} intensity
   * @param {boolean} dependant: when group == null treat the emission factor as being non independant
   *
   */
  _addIntensity (intensity, dependant = false) {
    if (intensity.group === null) {
      this._intensities.intensity += intensity.intensity

      if (dependant) {
        this._intensities.uncertainty += intensity.uncertainty
      } else {
        // independent case = quadratic sum
        // Note that we keep homogeneous result wrt the other case
        this._intensities.uncertainty = Math.sqrt(Math.pow(this._intensities.uncertainty, 2) + Math.pow(intensity.uncertainty, 2))
      }
    } else if (Object.keys(this._intensitiesPerMeta).includes(intensity.group)) {
      this._intensitiesPerMeta[intensity.group].intensity += intensity.intensity
      this._intensitiesPerMeta[intensity.group].uncertainty += intensity.uncertainty
    } else {
      this._intensitiesPerMeta[intensity.group] = new CarbonIntensity(
        intensity.intensity,
        intensity.uncertainty,
        intensity.group
      )
    }
  }

  /**
   * Add *in-place* this intensity to the collection.
   * @param {CarbonIntensity|CarbonIntensities|DetailedCarbonIntensity|DetailedCarbonIntensities} intensity
   *
   * NOTE: we should be able to add anything that coerces to a CarbonIntensity types.
   */
  add (intensity) {
    if (intensity instanceof CarbonIntensity) {
      this._addIntensity(intensity)
    } else if (intensity instanceof DetailedCarbonIntensity) {
      this._addIntensity(intensity.toCarbonIntensity())
    } else if (intensity instanceof CarbonIntensities) {
      this._addIntensity(intensity._intensities, true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key])
      }
    } else if (intensity instanceof DetailedCarbonIntensities) {
      this._addIntensity(intensity._intensities.toCarbonIntensity(), true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key].toCarbonIntensity())
      }
    }
  }

  /**
   * intensity
   * @type {number}
   * @readonly
   */
  get intensity () {
    let total = this._intensities.intensity
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += this._intensitiesPerMeta[key].intensity
    }
    return total
  }

  /**
   * uncertainty
   * @type {number}
   * @readonly
   */
  get uncertainty () {
    let total = Math.pow(this._intensities.uncertainty, 2)
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += Math.pow(this._intensitiesPerMeta[key].uncertainty, 2)
    }
    return Math.sqrt(total)
  }

  /**
   *
   * Sum-up all the element of the collection.
   * This actually coerces the collection to a CarbonIntensity whose group is null
   * The aggregated uncertainty is computed by
   * - first a linear sum of intensity within same group
   * - then a quadratic sum of the resulting intensities of each group
   *
   * @returns {CarbonIntensity} the sumed carbon intensity
   */
  sum () {
    let intensity = this.intensity
    let uncertainty = this.uncertainty
    return new CarbonIntensity(
      intensity,
      uncertainty
    )
  }
}

/** Collection of DetailledCarbonIntensity. */
export class DetailedCarbonIntensities {
  /**
   * Create and initialize the collection.
   */
  constructor () {
    this._intensities = new DetailedCarbonIntensity()
    this._intensitiesPerMeta = {}
  }

  /**
   *
   * @private
   * @param {DetailedCarbonIntensity} intensity
   * @param {boolean} dependant
   *
   */
  _addIntensity (intensity, dependant = false) {
    if (intensity.group === null) {
      this._intensities.co2 += intensity.co2
      this._intensities.ch4 += intensity.ch4
      this._intensities.n2o += intensity.n2o
      this._intensities.other += intensity.other
      this._intensities.combustion.intensity += intensity.combustion.intensity
      this._intensities.upstream.intensity += intensity.upstream.intensity
      this._intensities.manufacturing.intensity += intensity.manufacturing.intensity
      if (dependant) {
        this._intensities.combustion.uncertainty += intensity.combustion.uncertainty
        this._intensities.upstream.uncertainty += intensity.upstream.uncertainty
        this._intensities.manufacturing.uncertainty += intensity.manufacturing.uncertainty
      } else {
        // independent case = quadratic sum
        // Note that we keep homogeneous result wrt the other case
        this._intensities.combustion.uncertainty = Math.sqrt(Math.pow(this._intensities.combustion.uncertainty, 2) + Math.pow(intensity.combustion.uncertainty, 2))
        this._intensities.upstream.uncertainty = Math.sqrt(Math.pow(this._intensities.upstream.uncertainty, 2) + Math.pow(intensity.upstream.uncertainty, 2))
        this._intensities.manufacturing.uncertainty = Math.sqrt(Math.pow(this._intensities.manufacturing.uncertainty, 2) + Math.pow(intensity.manufacturing.uncertainty, 2))
      }
    } else if (Object.keys(this._intensitiesPerMeta).includes(intensity.group)) {
      this._intensitiesPerMeta[intensity.group].co2 += intensity.co2
      this._intensitiesPerMeta[intensity.group].ch4 += intensity.ch4
      this._intensitiesPerMeta[intensity.group].n2o += intensity.n2o
      this._intensitiesPerMeta[intensity.group].other += intensity.other
      this._intensitiesPerMeta[intensity.group].combustion.intensity += intensity.combustion.intensity
      this._intensitiesPerMeta[intensity.group].combustion.uncertainty += intensity.combustion.uncertainty
      this._intensitiesPerMeta[intensity.group].upstream.intensity += intensity.upstream.intensity
      this._intensitiesPerMeta[intensity.group].upstream.uncertainty += intensity.upstream.uncertainty
      this._intensitiesPerMeta[intensity.group].manufacturing.intensity += intensity.manufacturing.intensity
      this._intensitiesPerMeta[intensity.group].manufacturing.uncertainty += intensity.manufacturing.uncertainty
    } else {
      this._intensitiesPerMeta[intensity.group] = new DetailedCarbonIntensity(
        intensity.co2,
        intensity.ch4,
        intensity.n2o,
        intensity.other,
        intensity.combustion.intensity,
        intensity.combustion.uncertainty,
        intensity.upstream.intensity,
        intensity.upstream.uncertainty,
        intensity.manufacturing.intensity,
        intensity.manufacturing.uncertainty
      )
    }
  }

  /**
   * Add in-place this intensity to the collection.
   * @param {DetailedCarbonIntensity|DetailedCarbonIntensities} intensity
   */

  add (intensity) {
    if (intensity instanceof DetailedCarbonIntensity) {
      this._addIntensity(intensity)
    } else if (intensity instanceof DetailedCarbonIntensities) {
      this._addIntensity(intensity._intensities, true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key])
      }
    }
  }

  /**
   *
   * Sum up all the elements of the collection.
   * This actualy coerces this to a DetailledCarbonIntensity whose group is null.
   *
   * @returns {DetailedCarbonIntensity} the sumed carbon intensity
   *
   */
  sum () {
    let co2 = this._intensities.co2
    let ch4 = this._intensities.ch4
    let n2o = this._intensities.n2o
    let other = this._intensities.other
    let combustioni = this._intensities.combustion.intensity
    let combustionu = Math.pow(this._intensities.combustion.uncertainty, 2)
    let upstreami = this._intensities.upstream.intensity
    let upstreamu = Math.pow(this._intensities.upstream.uncertainty, 2)
    let manufacturingi = this._intensities.manufacturing.intensity
    let manufacturingu = Math.pow(this._intensities.manufacturing.uncertainty, 2)
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      co2 += this._intensitiesPerMeta[key].co2
      ch4 += this._intensitiesPerMeta[key].ch4
      n2o += this._intensitiesPerMeta[key].n2o
      other += this._intensitiesPerMeta[key].other
      combustioni += this._intensitiesPerMeta[key].combustion.intensity
      combustionu += Math.pow(this._intensitiesPerMeta[key].combustion.uncertainty, 2)
      upstreami += this._intensitiesPerMeta[key].upstream.intensity
      upstreamu += Math.pow(this._intensitiesPerMeta[key].upstream.uncertainty, 2)
      manufacturingi += this._intensitiesPerMeta[key].manufacturing.intensity
      manufacturingu += Math.pow(this._intensitiesPerMeta[key].manufacturing.uncertainty, 2)
    }
    return new DetailedCarbonIntensity(
      co2,
      ch4,
      n2o,
      other,
      combustioni,
      Math.sqrt(combustionu),
      upstreami,
      Math.sqrt(upstreamu),
      manufacturingi,
      Math.sqrt(manufacturingu)
    )
  }
}
