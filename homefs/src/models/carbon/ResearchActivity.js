/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import RACTIVITIES_FACTORS from '@/../data/emissionFactors/ractivitiesFactors.json'
import COMPUTING_FACILITIES_ALPHA from '@/../data/researchActivities/computingFacilitiesAlpha.json'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'
import { v4 as uuidv4 } from 'uuid'

// Depreciation periods (in years) for astronomical ground and space facilities
const DEPRECIATION = {
  'astro.ground': 38,
  'astro.space': 18
}
export class ResearchActivity {
  lang = {
    fr: {
      'subtype': 'Sous-type',
      'amount': 'Quantité'
    },
    en: {
      'subtype': 'Sub-type',
      'amount': 'Amount'
    }
  }

  constructor (id, category, type, subtype, amount = 0) {
    this.id = id
    this.uuid = uuidv4()
    this.category = category
    this.type = type
    this.subtype = subtype
    this.amount = amount
    this.intensity = new CarbonIntensity()
    this.items = []
  }

  get score () {
    return 1
  }

  get isGrouped () {
    return false
  }

  toString (sep = '\t') {
    return [
      this.category,
      this.type,
      this.subtype,
      this.amount,
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return this.score !== 1
  }

  isInvalid () {
    return this.score !== 1
  }

  getUnit () {
    let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[this.category][this.type][this.subtype])
    return ef.unit
  }

  toDatabase () {
    return {
      'category': this.category,
      'type': this.type,
      'subtype': this.subtype,
      'amount': this.amount
    }
  }

  getEmissionFactor (year) {
    let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[this.category][this.type][this.subtype])
    return ef.getFactor(year)
  }

  getCarbonIntensity (year = null) {
    let intensity = new CarbonIntensity()
    let ef = this.getEmissionFactor(year)
    intensity = new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  getCarbonIntensityGHGP (year = null) {
    return {
      '3.1': this.getCarbonIntensity(year)
    }
  }

  getCarbonIntensityV5 (year = null) {
    return {
      '4.5': this.getCarbonIntensity(year)
    }
  }

  static translate (ractivity, key, lang) {
    if (key in ractivity.lang[lang]) {
      return ractivity.lang[lang][key]
    } else {
      return key
    }
  }

  static getDescription (category, type, subtype, lang) {
    let key = 'description_' + lang.toUpperCase()
    let ef = RACTIVITIES_FACTORS[category][type][subtype]
    if (key in ef) {
      return ef[key]
    } else {
      return ef['description_EN']
    }
  }

  static getUnit (category, type, subtype) {
    let unit = '-'
    if (Object.keys(RACTIVITIES_FACTORS).includes(category)) {
      if (Object.keys(RACTIVITIES_FACTORS[category]).includes(type)) {
        if (subtype) {
          if (Object.keys(RACTIVITIES_FACTORS[category][type]).includes(subtype)) {
            let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[category][type][subtype])
            unit = ef.unit
          }
        } else {
          let firstSubtype = Object.keys(RACTIVITIES_FACTORS[category][type])[0]
          let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[category][type][firstSubtype])
          unit = ef.unit
        }
      }
    }
    return unit
  }

  static getCategories () {
    let categories = Object.keys(RACTIVITIES_FACTORS)
    return Array.from(new Set(categories))
  }

  static getTypes (category) {
    let types = Object.keys(RACTIVITIES_FACTORS[category])
    return Array.from(new Set(types))
  }

  static getSubTypes (category, type, year) {
    let subtypes = Object.keys(RACTIVITIES_FACTORS[category][type])
    return Array.from(new Set(subtypes))
  }

  static exportHeader (sep = '\t') {
    return [
      'category',
      'type',
      'subtype',
      'amount',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = ResearchActivity.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items) {
    return items.length > 0
  }

  static compute (items, year) {
    let intensities = new CarbonIntensities()
    let intensitiesGHGP = {}
    let intensitiesV5 = {}
    let meta = []
    for (let item of items) {
      let intensity = item.getCarbonIntensity(year)
      let intensityV5 = item.getCarbonIntensityV5(year)
      let intensityGHGP = item.getCarbonIntensityGHGP(year)
      for (let key of Object.keys(intensityGHGP)) {
        if (key in intensitiesGHGP) {
          intensitiesGHGP[key].add(intensityGHGP[key])
        } else {
          intensitiesGHGP[key] = new CarbonIntensities()
          intensitiesGHGP[key].add(intensityGHGP[key])
        }
      }
      for (let key of Object.keys(intensityV5)) {
        if (key in intensitiesV5) {
          intensitiesV5[key].add(intensityV5[key])
        } else {
          intensitiesV5[key] = new CarbonIntensities()
          intensitiesV5[key].add(intensityV5[key])
        }
      }
      intensities.add(intensity)
      meta.unshift({
        'category': item.category,
        'type': item.type,
        'subtype': item.subtype,
        'amount': item.amount,
        'intensity': intensity
      })
    }
    for (let key of Object.keys(intensitiesGHGP)) {
      intensitiesGHGP[key] = intensitiesGHGP[key].sum()
    }
    for (let key of Object.keys(intensitiesV5)) {
      intensitiesV5[key] = intensitiesV5[key].sum()
    }
    return {
      'intensity': intensities.sum(),
      'intensitiesGHGP': intensitiesGHGP,
      'intensitiesV5': intensitiesV5,
      'meta': meta
    }
  }
}

export class CERNFacility extends ResearchActivity {
  lang = {
    fr: {
      'subtype': 'Type d\'expérience',
      'amount': 'Quantité'
    },
    en: {
      'subtype': 'Type of experience',
      'amount': 'Amount'
    }
  }
}

export class GENCIComputingFacilities extends ResearchActivity {
  lang = {
    fr: {
      'subtype': 'Type de service de calcul',
      'amount': 'Quantité'
    },
    en: {
      'subtype': 'Type of computing service',
      'amount': 'Amount'
    }
  }

  getUnit () {
    let subtype = this.subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[this.category][this.type][subtype])
    return ef.unit
  }

  getEmissionFactor (year) {
    let efsubtype = this.subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS[this.category][this.type][efsubtype])
    return ef.getFactor(year)
  }

  getCarbonIntensity (year = null) {
    let intensity = new CarbonIntensity()
    let ef = this.getEmissionFactor(year)
    let alpha = GENCIComputingFacilities.ALPHA.filter(obj => obj.name === this.subtype)[0].alpha
    intensity = new CarbonIntensity(
      this.amount * ef.total.total * alpha,
      this.amount * ef.total.total * alpha * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  static getDescription (category, type, subtype, lang) {
    let alpha = GENCIComputingFacilities.ALPHA.filter(obj => obj.name === subtype)[0]
    if (alpha) {
      return alpha.description
    } else {
      return '-'
    }
  }

  static getUnit (category, type, subtype) {
    let rsubtype = subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(RACTIVITIES_FACTORS['facilities']['genci.computing.facilities'][rsubtype])
    return ef.unit
  }

  static getSubTypes (category, type, year) {
    return GENCIComputingFacilities.ALPHA
      .filter(obj => year >= obj['commissioning.year'] && year <= obj['downtime.year'])
      .map(obj => obj.name)
  }

  static get ALPHA () {
    return COMPUTING_FACILITIES_ALPHA
  }
}

export class AstroFacilities extends ResearchActivity {
  get isGrouped () {
    return true
  }

  getCarbonIntensity (year) {
    let intensity = new CarbonIntensity()
    const ef = this.getEmissionFactor(year)
    const start = RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['start']
    const stop = RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['stop']
    let construction = 0
    let operations = 0
    const astroCat = RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['class']
    if (year >= start && year - start <= DEPRECIATION[astroCat]) {
      construction = (this.amount / 100) * (ef.construction.total / DEPRECIATION[astroCat])
    }
    if (year >= start && year <= stop) {
      operations = (this.amount / 100) * ef.operations.total
    }
    intensity = new CarbonIntensity(
      construction + operations,
      construction * ef.construction.uncertainty + operations * ef.operations.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  getUnit () {
    if (this.subtype === '-') {
      return 'use'
    } else {
      return super.getUnit()
    }
  }

  static get headerRow () {
    let row = new AstroFacilities(
      null,
      'facilities',
      'astro',
      '-',
      '-'
    )
    return row
  }
}

export class FertilisersActivity extends ResearchActivity {
  lang = {
    fr: {
      'subtype': 'Type d\'engrais',
      'amount': 'Quantité'
    },
    en: {
      'subtype': 'Type of fertilizer',
      'amount': 'Amount'
    }
  }

  getCarbonIntensityGHGP (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      '3.1': new CarbonIntensity(
        this.amount * ef.manufacturing.total,
        this.amount * ef.manufacturing.total * ef.manufacturing.uncertainty,
        ef.group
      ),
      '1.3': new CarbonIntensity(
        this.amount * ef.spreading.total,
        this.amount * ef.spreading.total * ef.spreading.uncertainty,
        ef.group
      )
    }
  }

  getCarbonIntensityV5 (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      '4.1': new CarbonIntensity(
        this.amount * ef.manufacturing.total,
        this.amount * ef.manufacturing.total * ef.manufacturing.uncertainty,
        ef.group
      ),
      '1.3': new CarbonIntensity(
        this.amount * ef.spreading.total,
        this.amount * ef.spreading.total * ef.spreading.uncertainty,
        ef.group
      )
    }
  }
}

export function researchActivityClass (type) {
  let classes = {
    'cern': CERNFacility,
    'genci.computing.facilities': GENCIComputingFacilities,
    'astro': AstroFacilities,
    'fertilisers': FertilisersActivity
  }
  if (Object.keys(classes).includes(type)) {
    return classes[type]
  } else {
    return ResearchActivity
  }
}

export function researchActivityFactory (item) {
  let Nclass = researchActivityClass(item.type)
  let obj = new Nclass(
    item.id,
    item.category,
    item.type,
    item.subtype,
    item.amount
  )
  return obj
}
