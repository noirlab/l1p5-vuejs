import { researchActivityFactory } from './ResearchActivity'

function cernLHC () {
  return researchActivityFactory({
    id: null,
    category: 'facilities',
    type: 'cern',
    subtype: 'with.lhc',
    amount: 1
  })
}

function cernWLHC () {
  return researchActivityFactory({
    id: null,
    category: 'facilities',
    type: 'cern',
    subtype: 'without.lhc',
    amount: 1
  })
}

describe('Compute cern emissions', () => {
  test('with.lhc', () => {
    let ractivity = cernLHC()
    expect(ractivity.getCarbonIntensity(2023).intensity).toBe(25120)
    expect(ractivity.getUnit()).toBe('user')
  })

  test('without.lhc', () => {
    let ractivity = cernWLHC()
    expect(ractivity.getCarbonIntensity(2023).intensity).toBe(9380)
    expect(ractivity.getUnit()).toBe('user')
  })
})

describe('Export research activities', () => {
  test('with.lhc', () => {
    let ractivity = cernLHC()
    expect(ractivity.toString()).toEqual('facilities\tcern\twith.lhc\t1\t0\t0')
  })
  test('without.lhc', () => {
    let ractivity = cernWLHC()
    expect(ractivity.toString()).toEqual('facilities\tcern\twithout.lhc\t1\t0\t0')
  })
})
