import Meal from './Meal'

describe('getCarbonIntensity', () => {
  test('Vegan meal', () => {
    let meal = Meal.createFromObj({
      'type': 'vegan',
      'amount': 5
    })
    expect(meal.getCarbonIntensity(2021).intensity).toBe(0.785 * 5)
    expect(meal.getCarbonIntensity(2021).uncertainty).toBe(0.785 * 0.5 * 5)
  })
  test('Classic meal with meet 2', () => {
    let meal = Meal.createFromObj({
      'type': 'classic.meet2',
      'amount': 5
    })
    expect(meal.getCarbonIntensity(2021).intensity).toBe(5.51 * 5)
    expect(meal.getCarbonIntensity(2021).uncertainty).toBe(5.51 * 0.5 * 5)
  })
})
