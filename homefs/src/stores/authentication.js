/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { defineStore } from 'pinia'
import { useAdminStore } from './admin'
import { useSuperadminStore } from './superadmin'
import { useGHGIStore } from './ghgi'
import { useScenarioStore } from './scenario'

import authenticationService from '@/services/authenticationService'
import axios from 'axios'
import router from '@/router'
import { useTransitionStore } from './transition'

export const useAuthenticationStore = defineStore('authentication', {
  state: () => {
    return {
      token: localStorage.getItem('user-token') || '',
      status: '',
      isSuperUser: false,
      isAuthModalOpen: false,
      authLink: null
    }
  },
  getters: {
    isAuthenticated: (state) => !!state.token,
    authStatus: (state) => state.status
  },

  actions: {
    resetAllStore () {
      const admin = useAdminStore()
      const superadmin = useSuperadminStore()
      const ghgi = useGHGIStore()
      const scenario = useScenarioStore()
      const transition = useTransitionStore()
      admin.$reset()
      superadmin.$reset()
      ghgi.$reset()
      scenario.$reset()
      transition.$reset()
    },
    authRequest (user) {
      return authenticationService.authenticate(user)
        .then(data => {
          const token = data.access
          localStorage.setItem('user-token', token)
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
          this.status = 'success'
          this.token = token
          this.resetAllStore()
          this.checkIsSuperUser()
          router.push(this.authLink).catch(() => { })
          return data
        })
        .catch(error => {
          this.status = 'error'
          localStorage.removeItem('user-token')
          throw error
        })
    },
    authLogout () {
      return new Promise((resolve, reject) => {
        this.token = ''
        delete axios.defaults.headers.common['Authorization']
        localStorage.removeItem('user-token')
        this.resetAllStore()
        router.push('/').catch(() => {})
        resolve()
      })
    },
    checkIsSuperUser () {
      return authenticationService.isSuperUser()
        .then(data => {
          this.isSuperUser = data.is_super_user
          return data.is_super_user
        })
        .catch(error => {
          this.isSuperUser = false
          throw error
        })
    },
    openAuthModal (authLink) {
      this.isAuthModalOpen = true
      this.authLink = authLink
    },
    closeAuthModal () {
      this.isAuthModalOpen = false
      this.authLink = null
    }
  }
})
