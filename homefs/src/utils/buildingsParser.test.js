import Modules from '@/models/Modules.js'
import { readFile } from 'fs/promises'
import { parseFile } from './buildingsParser'

const path = require('node:path')

describe('parseBuldings', () => {
  test('buildingsTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/buildingTemplate.csv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([filetype, content, headerMap, error]) => {
        expect(filetype).toBe('energies')
        expect(error).toBe('ok')
        expect(content.length).toBe(9)

        // sanity check on the first building (only)
        expect(content[0][headerMap.name]).toBe('B1')
        expect(content[0][headerMap.area]).toBe('4500')
        expect(content[0][headerMap.share]).toBe('50')
        expect(content[0].site).toBe('Toulouse')
        expect(content[0][headerMap.electricity]).toBe('100000')
        expect(content[0][headerMap.water]).toBe('1000')
        expect(content[0][headerMap.selfConsumption]).toBe('')
        expect(content[0][headerMap.heatings.electric]).toBe('')
        expect(content[0][headerMap.heatings.urbanNetwork]).toBe('')
        expect(content[0][headerMap.heatings.urbanConsumption]).toBe('')
        expect(content[0][headerMap.heatings.naturalgas]).toBe('50000')
        expect(content[0][headerMap.heatings.propane]).toBe('')
        expect(content[0][headerMap.heatings['heating.oil']]).toBe('1000')
        expect(content[0][headerMap.heatings.biomethane]).toBe('')
        expect(content[0][headerMap.heatings['wood.pellets']]).toBe('')
        expect(content[0][headerMap.heatings['wood.chips']]).toBe('400')
        expect(content[0][headerMap.heatings['wood.logs']]).toBe('')
        expect(content[0][headerMap.isOwned]).toBe('gaz,fioul')
      })
  })

  test('buildingsTemplate.tsv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/buildingTemplate.tsv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([filetype, content, headerMap, error]) => {
        expect(filetype).toBe('energies')
        expect(error).toBe('ok')
        expect(content.length).toBe(9)

        // sanity check on the first building (only)
        expect(content[0][headerMap.name]).toBe('B1')
        expect(content[0][headerMap.area]).toBe('4500')
        expect(content[0][headerMap.share]).toBe('50')
        expect(content[0].site).toBe('Toulouse')
        expect(content[0][headerMap.electricity]).toBe('100000')
        expect(content[0][headerMap.water]).toBe('1000')
        expect(content[0][headerMap.selfConsumption]).toBe('')
        expect(content[0][headerMap.heatings.electric]).toBe('')
        expect(content[0][headerMap.heatings.urbanNetwork]).toBe('')
        expect(content[0][headerMap.heatings.urbanConsumption]).toBe('')
        expect(content[0][headerMap.heatings.naturalgas]).toBe('50000')
        expect(content[0][headerMap.heatings.propane]).toBe('')
        expect(content[0][headerMap.heatings['heating.oil']]).toBe('1000')
        expect(content[0][headerMap.heatings.biomethane]).toBe('')
        expect(content[0][headerMap.heatings['wood.pellets']]).toBe('')
        expect(content[0][headerMap.heatings['wood.chips']]).toBe('400')
        expect(content[0][headerMap.heatings['wood.logs']]).toBe('')
        expect(content[0][headerMap.isOwned]).toBe('gaz,fioul')
      })
  })
})

describe('parseBuldings', () => {
  test('buildingsTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/refrigerantTemplate.csv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([filetype, content, headerMap, error]) => {
        expect(filetype).toBe(Modules.REFRIGERANTS)
        expect(error).toBe('ok')
        expect(content.length).toBe(8)
        expect(content[0][headerMap.name]).toBe('B1')
        expect(content[0][headerMap.refrigerants.name]).toBe('R116')
        expect(content[0][headerMap.refrigerants.total]).toBe('1')
      })
  })

  test('buildingsTemplate.tsv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/refrigerantTemplate.tsv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([filetype, content, headerMap, error]) => {
        expect(filetype).toBe(Modules.REFRIGERANTS)
        expect(error).toBe('ok')
        expect(content.length).toBe(8)
        expect(content[0][headerMap.name]).toBe('B1')
        expect(content[0][headerMap.refrigerants.name]).toBe('R116')
        expect(content[0][headerMap.refrigerants.total]).toBe('1')
      })
  })
})
