import Vue from 'vue'

Vue.filter('splitFloat', function (value) {
  return value
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, '\xa0')
})

Vue.filter('splitNumber', function (value) {
  return value
    .toFixed(0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, '\xa0')
})

Vue.filter('splitNumberTons', function (value) {
  return (value / 1000)
    .toFixed(2)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, '\xa0')
})

Vue.filter('convertFileSize', function (value) {
  if (value) {
    return Math.round(parseInt(value) / Math.pow(2, 10) * 100) / 100 + ' Ko'
  } else {
    return '0 ko'
  }
})

Vue.filter('convertFileSizeMo', function (value) {
  if (value) {
    return Math.round(parseInt(value) / Math.pow(2, 20) * 100) / 100 + ' Mo'
  } else {
    return '0 Mo'
  }
})

Vue.filter('dateFormat', function (value) {
  let d = new Date(value)
  let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
  return d.toLocaleDateString('fr-FR', options)
})

Vue.filter('truncate', function (value, end) {
  if (value.length > end + 3) {
    return (value.substring(0, end) + '...')
  } else {
    return value
  }
})

Vue.filter('capitalize', function (value) {
  return (value.charAt(0).toUpperCase() + value.slice(1).toLowerCase())
})
