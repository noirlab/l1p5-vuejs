/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import axios from 'axios'

function escapeVille (ville) {
  let eVille = ville.toLowerCase()
  eVille = eVille.replace(/^st\s/gi, 'saint ')
  eVille = eVille.replace('cedex', '')
  eVille = eVille.replace('#', '%23')
  eVille = eVille.replace('?', '%3F')
  eVille = eVille.replace('&', '%26')
  return eVille
}

function searchGEONames (userName, ville, country = '', maxRows = 10, fuzzy = 0.6) {
  const geoAxios = axios.create()
  geoAxios.defaults.headers.common = {}
  geoAxios.defaults.headers.common.accept = 'application/json'
  let request = 'https://secure.geonames.net/searchJSON?name=' + escapeVille(ville)
  if (country) {
    if (Array.isArray(country)) {
      for (let ccountry of country) {
        request += '&country=' + ccountry
      }
    } else {
      request += '&country=' + country
    }
  }
  /*
  Filters for following feature classes
    A (country, state, region,...)
    P city, village,...
    S spot, building, farm
  */
  request += '&featureClass=A&featureClass=P&featureClass=S'
  // request += '&countryBias=FR'
  request += '&maxRows=' + maxRows + '&fuzzy=' + fuzzy + '&lang=fr&username=' + userName
  return geoAxios.get(request, { headers: {} })
}

function countryInfo (userName, lang) {
  const geoAxios = axios.create()
  geoAxios.defaults.headers.common = {}
  geoAxios.defaults.headers.common.accept = 'application/json'
  let request = 'https://secure.geonames.net/countryInfo?lang=' + lang + '&username=' + userName
  return geoAxios.get(request, { headers: {} })
}

export { escapeVille, searchGEONames, countryInfo }
