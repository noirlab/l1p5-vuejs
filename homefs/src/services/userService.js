import api from '@/services/api'

export default {
  resetPassword (payload) {
    return api.post(`reset_password/`, payload)
      .then(response => response.data)
  },
  userExists (payload) {
    return api.post(`user_exists/`, payload)
      .then(response => response.data)
  },
  getUsers () {
    return api.get(`get_users/`)
      .then(response => response.data)
  },
  updateIsAdmin (payload) {
    return api.post(`update_is_admin/`, payload)
      .then(response => response.data)
  },
  updateUser (payload) {
    return api.post(`update_user/`, payload)
      .then(response => response.data)
  },
  deleteUser (payload) {
    return api.post(`delete_user/`, payload)
      .then(response => response.data)
  },
  sendActivationEmail (payload) {
    return api.post(`send_activation_email/`, payload)
      .then(response => response.data)
  },
  /**
   *
   * Update the roles of a single user
   *
   * payload :
   * {
   *    email: <email>,
   *    value: ["reviewer"]
   * }
   *
   * Nov 2023: Note that reviewer is the only value accepted
   *
   * @param {*} payload
   *
   * @returns  the freshly updated user
   */
  updateRoles (payload) {
    return api.post(`update_roles/`, payload)
      .then(response => response.data)
  }
}
