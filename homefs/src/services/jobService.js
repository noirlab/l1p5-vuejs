import api from '@/services/api'

export default {
  getJobs () {
    return api.get(`/jobs/`).then((response) => response.data)
  },
  getJob (jobId) {
    return api.get(`/jobs/${jobId}/`).then((response) => response.data)
  },
  launchJob (task, origin) {
    return api.post(`/jobs/`, { task, origin }).then((response) => response.data)
  },
  deleteJob (jobId) {
    return api.delete(`/jobs/${jobId}/`).then((response) => response.data)
  }
}
