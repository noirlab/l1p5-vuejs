import datetime
import logging
from typing import Optional

from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.http import FileResponse
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.pagination import PageNumberPagination

from backend.transition.utils import (
    can_act,
    can_get,
    notify_publish_action,
    notify_reviewer_add_message,
    notify_submit_action,
    notify_user_add_message,
)
from ..core.models import Laboratory

from ..carbon.models import GHGI

from ..users.models import L1P5User

from .models import (
    Message,
    PublicAction,
    PublicActionFile,
    Tag,
    global_quota,
    validate_filename,
    CAN_REVIEW,
    DRAFT,
    SUBMITTED,
    PUBLISHED,
)
from .serializers import (
    MessageSerializer,
    PublicActionSerializer,
    SearchActionSerializer,
    SimpleLaboratorySerializer,
)


logger = logging.getLogger(__name__)

# 10MB quota (hardcoded here in purpose)
ONE_MB = 2**20
QUOTA = 5 * ONE_MB


def build_files_to_save(files, quota):
    # filter out some files
    # This is done silently for now
    current_quota = quota
    files_to_save = []
    for (name, file) in files.items():
        try:
            validate_filename(name)
            current_quota += file.size
            if current_quota > QUOTA:
                raise ValidationError("Quota exceeded")
        except ValidationError as e:
            print(f"Skipping {name} : ", e)
            continue
        files_to_save.append((name, file))

    return files_to_save


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_count(request):
    laboratory = get_object_or_404(Laboratory, referent_id=request.user.id)
    count = PublicAction.objects.filter(laboratory_id=laboratory.id).count()
    return Response(dict(count=count), status=status.HTTP_200_OK)


@api_view(["GET"])
@renderer_classes([JSONRenderer])
@permission_classes([IsAuthenticated])
def get_transition_quota(request):
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        quota = global_quota(laboratory.id)
        data = dict(current=quota, total=QUOTA)
    except Exception as e:
        logger.error(e)
        return Response(dict(current=0, total=QUOTA), status=status.HTTP_200_OK)
    return Response(data, status=status.HTTP_200_OK)


def _update_or_create(laboratory: Laboratory, action: Optional[dict]):
    """update or create an action.

    Beware on the side-effects on action

    Bonus: no-op for empty action dict
    """
    action_id = action.pop("id", None)
    # strip any lab_id
    action.pop("laboratory", None)
    # and reviewer (as this is used as shortcut when sending data to the frontend)
    action.pop("reviewer", None)
    # this doesn't call save so resetting the admin status manually
    action.update(admin_status=DRAFT)

    # update the available tag list
    tags = set()
    tag_descriptors = action.pop("tags", [])
    for descriptor in tag_descriptors:
        tag, _ = Tag.objects.get_or_create(descriptor=descriptor)
        tags.add(tag)

    new_action, _ = PublicAction.objects.update_or_create(
        id=action_id, laboratory_id=laboratory.id, defaults=action
    )
    new_action.tags.set(tags)

    return new_action


@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
def mgmt_public_actions(request):
    laboratory = request.user.laboratory.first()
    # FIXME(msimonin): should we get a 404 here
    # I think we kept on using 200 for some compatibility with the frontend code
    if laboratory is None:
        return Response(None, status=status.HTTP_200_OK)
    if request.method == "GET":
        actions = PublicAction.objects.prefetch_related("laboratory").filter(
            laboratory_id=laboratory.id
        )
        serializer = PublicActionSerializer(
            actions, many=True, context={"request": None}
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "POST":
        new_action = _update_or_create(laboratory, request.data.copy())
        serializer = PublicActionSerializer(new_action, context={"request": None})
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET", "POST", "DELETE"])
@permission_classes([IsAuthenticated])
def mgmt_public_action(request, action_id):

    a = PublicAction.objects.get(id=action_id)
    if not can_get(request.user, a, allow_public=True):
        raise PermissionDenied()

    if request.method == "DELETE" and can_act(request.user, a):
        a.delete()
        return Response(None, status=status.HTTP_200_OK)

    if request.method == "POST" and can_act(request.user, a):
        # submit the action
        a.admin_status = SUBMITTED
        a.save(update_fields=["admin_status"])
        notify_submit_action(request, a)
        serializer = PublicActionSerializer(a, context={"request": None})
        return Response(serializer.data)

    if request.method == "GET":
        serializer = PublicActionSerializer(a, context={"request": None})
        return Response(serializer.data, status=status.HTTP_200_OK)

    raise PermissionDenied()


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def mgmt_public_files(request, action_id):
    files = request.FILES
    # get the action corresponding to the action id and owned transition lab
    # this should be sufficient to check that we're modifying a owned action

    action = PublicAction.objects.get(id=action_id)

    if not can_act(request.user, action):
        return Response(None, status=status.HTTP_403_FORBIDDEN)

    files_to_save = build_files_to_save(files, global_quota(action.laboratory.id))
    try:
        for (name, file) in files_to_save:
            # FIXME(msimonin): this creates a copy on the fs with a different suffix
            # that's the expected behaviour of the FileSystemStorage (generate a
            # new name if a file with the same name already exists)
            af, _ = PublicActionFile.objects.update_or_create(
                action=action, name=name, defaults=dict(file=file)
            )
    except Exception as e:
        logger.error(e)
        return Response(
            "Something went wrong when uploading the files",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    # refresh
    action = PublicAction.objects.get(id=action_id)
    serializer = PublicActionSerializer(action, context={"request": None})
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def reviewer_get_quota(request, laboratory_id):
    lab = get_object_or_404(Laboratory, id=laboratory_id)
    if request.user != lab.referent and not request.user.has_perm(CAN_REVIEW):
        raise PermissionDenied
    quota = global_quota(lab.id)
    data = dict(current=quota, total=QUOTA)
    return Response(data, status=status.HTTP_200_OK)


def get_delete_file(request, af):
    """Works for action file of public file"""
    if request.method == "GET" and can_get(request.user, af, allow_public=True):
        # return the blob with correct content type
        return FileResponse(af.file.open())

    if not request.user.is_authenticated:
        return Response(None, status=status.HTTP_401_UNAUTHORIZED)

    if request.method == "DELETE" and can_act(request.user, af):
        af.delete()
        return Response(None, status=status.HTTP_200_OK)

    raise PermissionDenied()


@api_view(["GET", "DELETE"])
@permission_classes([])
def mgmt_public_file(request, action_id, file_id):
    af = None
    try:
        af = PublicActionFile.objects.get(id=file_id)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    assert af is not None

    # We fall here in several situations
    # esp. we looking a published action while being anonymous
    # because our hyper link serializer generates internal links only
    # so we need to check twice who can read the file
    return get_delete_file(request, af)


def filter_items(cls, request):
    """
    Filter Plans or PublicActions according to their common fields:
    - laboratory__disciplines
    - laboratory__admnistrations
    - laboratory__latlng
    - laboratory__admin_status

    """
    disciplines = request.query_params.getlist("disciplines[]")
    disciplines_op = request.query_params.get("disciplinesOp", "AND")
    administrations = request.query_params.getlist("administrations[]")
    administrations_op = request.query_params.get("administrationsOp", "AND")
    tags = request.query_params.getlist("tags[]")
    tags_op = request.query_params.get("tagsOp", "AND")

    bounds = request.query_params.getlist("bounds[]")
    sort = request.query_params.get("sort")
    status = request.query_params.get("status")
    title = request.query_params.get("title")
    laboratory = request.query_params.get("laboratory")

    items = (
        cls.objects.prefetch_related(
            "laboratory__disciplines", "laboratory__administrations", "tags", "files"
        )
        .prefetch_related("laboratory__referent__groups", "reviewer__groups")
        .filter(admin_status=PUBLISHED)
    )

    if disciplines:
        if not disciplines_op or disciplines_op == "AND":
            for discipline in disciplines:
                items = items.filter(laboratory__disciplines__id=discipline)
        else:
            # there's at least one discipline
            q = Q(laboratory__disciplines__id=disciplines[0])
            for discipline in disciplines[1:]:
                q = q | Q(laboratory__disciplines__id=discipline)
            items = items.filter(q)
        items = items.distinct()

    if administrations:
        if not administrations_op or administrations_op == "AND":
            for administration in administrations:
                items = items.filter(
                    laboratory__administrations__name__icontains=administration
                )
        else:
            q = Q(laboratory__administrations__name__icontains=administrations[0])
            for administration in administrations:
                q = q | Q(laboratory__administrations__name__icontains=administrations)
            items = items.filter(q)
        items = items.distinct()

    if tags:
        if not tags_op or tags_op == "AND":
            for tag in tags:
                items = items.filter(tags__descriptor=tag)
        else:
            q = Q(tags__descriptor=tags[0])
            for tag in tags:
                q = q | Q(tags__descriptor=tag)
            items = items.filter(q)
        items = items.distinct()

    if bounds:
        lat1, lon1, lat2, lon2 = bounds
        items = items.filter(
            laboratory__latitude__gte=float(lat1),
            laboratory__longitude__gte=float(lon1),
            laboratory__latitude__lte=float(lat2),
            laboratory__longitude__lte=float(lon2),
        )

    if sort == "sort:creation:asc":
        items = items.order_by("created")
    elif sort == "sort:creation:desc":
        items = items.order_by("-created")
    elif sort == "sort:update:asc":
        items = items.order_by("last_update")
    elif sort == "sort:update:desc":
        items = items.order_by("-last_update")

    if status == "status:running":
        items = items.filter(Q(end__isnull=True) | Q(end__gte=datetime.datetime.now()))
    elif status == "status:ended":
        items = items.filter(Q(end__isnull=False) | Q(end__lte=datetime.datetime.now()))
    elif status == "status:abandoned":
        items = items.filter(abandoned=True)

    if title:
        items = items.filter(title__icontains=title)

    if laboratory:
        items = items.filter(laboratory__name__icontains=laboratory)

    # get unique items at then end
    return items.distinct()


@api_view(["GET"])
@permission_classes([])
@renderer_classes([JSONRenderer])
def public_actions_published(request):
    """Get the list of matching actions.

    This mainly used for displaying the search map/table.
    """

    actions = filter_items(PublicAction, request)

    paginator = PageNumberPagination()
    paginator.page_size = 1000
    result_page = paginator.paginate_queryset(actions, request)
    serializer = SearchActionSerializer(
        result_page, many=True, context={"request": None}
    )
    return paginator.get_paginated_response(serializer.data)


@api_view(["GET"])
@permission_classes([])
@renderer_classes([JSONRenderer])
def public_action_published(request, action_id):
    """Get one action published or not if the user has sufficient permissions."""

    action = get_object_or_404(PublicAction, id=action_id)
    if not can_get(request.user, action, allow_public=True):
        return Response(None, status=status.HTTP_403_FORBIDDEN)

    # all clear, proceed
    # in this case we want to generate only public links to the files
    action_serializer = PublicActionSerializer(action, context=dict(request=None))
    return Response(action_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@renderer_classes([JSONRenderer])
@permission_required([CAN_REVIEW], raise_exception=True)
def reviewer_actions_submitted(request):
    """Get all actions that have been submitted"""
    public_actions = (
        PublicAction.objects.filter(admin_status=SUBMITTED)
        .prefetch_related(
            "laboratory",
            "laboratory__administrations",
            "laboratory__disciplines",
            "files",
        )
        .order_by("laboratory_id")
    )

    serializer = PublicActionSerializer(
        public_actions, many=True, context={"request": None}
    )

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_required([CAN_REVIEW], raise_exception=True)
@renderer_classes([JSONRenderer])
def reviewer_my_actions(request):
    data = request.data
    action_id = data.pop("id")
    action = get_object_or_404(PublicAction, id=action_id)
    action.reviewer = request.user
    # don't update the admin_status field
    action.save(update_fields="admin_status")
    serializer = PublicActionSerializer(action, context={"request": None})
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET", "POST", "PUT"])
@renderer_classes([JSONRenderer])
def reviewer_action(request, action_id):
    """Called when reviewing an action called by a reviewer or the action owner (PUT).

    - Toggle status(reviewer only)
    - Update an action while reviewing (user or reviewer)
        and this can be done on already published action
    - get the action (reviewer + user)
    """
    action = get_object_or_404(PublicAction, id=action_id)

    if request.method == "POST" and request.user.has_perm(CAN_REVIEW):
        # toggle status
        if action.admin_status == DRAFT:
            return Response(None, status=status.HTTP_403_FORBIDDEN)
        # toggle
        if action.admin_status == SUBMITTED:
            action.admin_status = PUBLISHED
        elif action.admin_status == PUBLISHED:
            action.admin_status = SUBMITTED
        action.save(update_fields=["admin_status"])
        notify_publish_action(request, action)
        serializer = PublicActionSerializer(action, context={"request": None})
        return Response(serializer.data)

    if request.method == "PUT" and can_act(request.user, action):
        admin_status = action.admin_status
        # get the laboratory of the action (must be set)
        new_action = _update_or_create(action.laboratory, request.data.copy())
        # if we aren' the owner (meaning we're reviewing other people action)
        if request.user != action.owner:
            new_action.admin_status = admin_status
            new_action.save(update_fields=["admin_status"])
        serializer = PublicActionSerializer(new_action, context={"request": None})
        return Response(serializer.data)

    if request.method == "GET" and can_get(request.user, action):
        serializer = PublicActionSerializer(action, context={"request": None})
        return Response(serializer.data)

    raise PermissionDenied()


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def reviewer_get_laboratory(request, laboratory_id):
    lab = get_object_or_404(Laboratory, id=laboratory_id)
    if request.user != lab.referent and not request.user.has_perm(CAN_REVIEW):
        raise PermissionDenied()
    serializer = SimpleLaboratorySerializer(lab)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
def has_reviewer_permissions(request):
    if request.user.is_authenticated:
        user = L1P5User.objects.get(email=request.user)
        return Response({"has_reviewer_permissions": user.has_perm(CAN_REVIEW)})

    return Response({"has_reviewer_permissions": False})


@api_view(["GET"])
@permission_classes([])
def has_submitted_ghgis(request):
    """Check if the user has at least one submitted GHGI module."""

    # assuming the user has a laboratory
    result = False
    try:
        laboratory = request.user.laboratory.first()
        # get all ghgis and get the submitted modules
        ghgis = GHGI.objects.filter(Q(laboratory__id=laboratory.id))
        submitteds = [ghgi.submitted for ghgi in ghgis]
        # at least one ghgi has at least one submitted module
        result = any([any(s.values()) for s in submitteds if s is not None])
        return Response({"has_submitted_ghgis": result})
    except Exception as e:
        logger.error("Error when checking submitted ghgis:", e)
        result = False

    return Response({"has_submitted_ghgis": result})


@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def reviewer_messages(request, action_id):
    """Used by a reviewer or a user"""
    action = get_object_or_404(PublicAction, id=action_id)

    if not can_act(request.user, action):
        raise PermissionDenied

    if request.method == "GET":
        messages = Message.objects.filter(action__id=action.id)
        serializer = MessageSerializer(messages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "POST":
        # infer origin
        origin = request.user.email
        # use contact email if the request user is the owner
        if action.owner == request.user:
            origin = action.display_contact
            notify_user_add_message(request, action)
        else:
            notify_reviewer_add_message(request, action)
        message = Message.objects.create(action=action, origin=origin, **request.data)
        serializer = MessageSerializer(message)
        return Response(serializer.data, status=status.HTTP_200_OK)
