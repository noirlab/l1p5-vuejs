from rest_framework import serializers
from rest_framework.reverse import reverse
from .models import Message, PublicAction, PublicActionFile, Tag

from ..core.serializers import AdministrationSerializer, DisciplineSerializer
from ..core.models import Laboratory

from ..users.serializers import L1P5UserSerializer


class MyHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            "plan_id": obj.action.plan.id,
            "action_id": obj.action.id,
            "file_id": obj.id,
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class MyPublicHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {"action_id": obj.action.id, "file_id": obj.id}
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class PublicActionFileSerializer(serializers.ModelSerializer):
    # https://www.django-rest-framework.org/api-guide/relations/#custom-hyperlinked-fields
    # HyperlinkedIdentityField is for itself
    url = MyPublicHyperlinkedIdentityField(view_name="mgmt-public-file")
    # align with javascript file size  attribute
    size = serializers.SerializerMethodField()

    def get_size(self, obj):
        return obj.file.size

    class Meta:
        model = PublicActionFile
        fields = "__all__"


class SimpleLaboratorySerializer(serializers.ModelSerializer):
    referent = L1P5UserSerializer()
    administrations = AdministrationSerializer(many=True)
    disciplines = DisciplineSerializer(many=True)
    contact = serializers.SerializerMethodField()

    def get_contact(self, obj):
        return obj.referent.email

    class Meta:
        model = Laboratory
        fields = [
            "contact",
            "id",
            "name",
            "referent",
            "administrations",
            "latitude",
            "longitude",
            "disciplines",
        ]


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class PublicActionSerializer(serializers.ModelSerializer):
    files = PublicActionFileSerializer(many=True, read_only=True)
    # shortcut: just present the email to the frontend
    reviewer = serializers.SerializerMethodField()
    laboratory = SimpleLaboratorySerializer()
    tags = TagSerializer(many=True, read_only=True)

    def get_reviewer(self, obj):
        if obj.reviewer is None:
            return None
        return obj.reviewer.email

    class Meta:
        model = PublicAction
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


class SearchLaboratorySerializer(serializers.ModelSerializer):
    """Rationale: get the bare minimal for displaying the search map/table.

    Avoid going through related field to deeply.
    e.g reviewer and referent requires to fetch the auth group for each record
    which might be slow.
    """

    class Meta:
        model = Laboratory
        fields = [
            "id",
            "name",
            "latitude",
            "longitude",
        ]


class SearchActionSerializer(serializers.ModelSerializer):
    """Used for displaying the search map/table of actions."""

    laboratory = SearchLaboratorySerializer()
    files = PublicActionFileSerializer(many=True, read_only=True)
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = PublicAction
        fields = "__all__"
