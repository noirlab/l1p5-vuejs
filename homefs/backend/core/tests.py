from django.test import Client
from django.test import TestCase
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken

from backend.test_utils import ensure_user_created


REQUEST_KWARGS = dict(content_type="application/json")


def make_payload(administrations):
    payload = dict(
        name="test_add_same_administrations",
        latitude=42.0,
        longitude=43.0,
        sites=["test_site"],
        disciplines=[],
        administrations=administrations,
    )
    return payload


class TestAddAdministrations(TestCase):
    def setUp(self) -> None:
        self.user = ensure_user_created("test@test.org")
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_add_same_administrations(self):
        r = self.api.post(
            "/api/save_laboratory/",
            make_payload(["plop", "plop", "plop"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # check the returned administrations
        response = r.json()
        self.assertEqual(
            1,
            len(response["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def test_add_same_administrations_case_insensitive(self):
        r = self.api.post(
            "/api/save_laboratory/",
            make_payload(["plop", "PLOP", "PlOp"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # check the returned administrations
        response = r.json()
        self.assertEqual(
            1,
            len(response["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def test_add_update_lab_administrations_case_insensitive(self):
        r = self.api.post(
            "/api/save_laboratory/",
            make_payload(["plop"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # Update the lab with some new (same) administrations
        response = r.json()

        payload = dict(**response)
        payload["administrations"].append("PLOP")

        r = self.api.post(
            "/api/save_laboratory/",
            payload,
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        response = r.json()
        self.assertEqual(
            1,
            len(response["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def tearDown(self) -> None:
        self.user.delete()
