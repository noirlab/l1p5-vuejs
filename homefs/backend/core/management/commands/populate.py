import random
from typing import Any, Iterable, List
from django.core.management.base import BaseCommand, CommandError

from backend.test_utils import create_test_users
from backend.core.models import Administration, Discipline, LaboratoryDiscipline
from backend.users.models import L1P5User


def populate_transition(users: List[L1P5User]):
    from backend.transition.models import DRAFT, SUBMITTED, PUBLISHED, PublicAction, Tag

    STATUSES = [DRAFT, SUBMITTED, PUBLISHED]
    TAGS = [
        "tag:ges:buildings",
        "tag:ges:devices",
        "tag:ges:travels",
    ]

    LOREM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non
risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies
sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim
est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu
massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut
in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue.
Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed
dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci
luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed
pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales
hendrerit."""

    for i_user, user in enumerate(users):
        laboratory = user.laboratory.first()
        for i in range(20):
            tags_descriptors = list(
                set(
                    [TAGS[i % len(TAGS)]]
                    + [TAGS[i_user % len(TAGS)]]
                    + [TAGS[(i_user + 1) % len(TAGS)]]
                )
            )
            public_action = PublicAction(
                laboratory=laboratory,
                title=f"Public action {i} - {laboratory.name}",
                text=f"Action {i}. {LOREM}",
                admin_status=STATUSES[i % len(STATUSES)],
            )
            tags = set()
            for descriptor in tags_descriptors:
                tag, _ = Tag.objects.get_or_create(descriptor=descriptor)
                tags.add(tag)
            public_action.save(update_fields="admin_status")
            public_action.tags.set(tags)


def populate_carbon(users):
    from backend.carbon.models import CommuteSection, GHGI, SurveyAnswer

    for user in users:
        l = user.laboratory.first()
        g = GHGI(
            laboratory=l,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        g.save()

        km = 20
        nWorkingDay = 1
        for status in ["researcher", "researcher", "engineer", "student"]:
            s = SurveyAnswer(ghgi=g, position=status, nWorkingDay=nWorkingDay)
            s.save()
            cs = CommuteSection(survey=s, mode="car", engine="diesel", distance=km)
            cs.save()

        # create a ghgi
        g = GHGI(
            laboratory=l,
            year=2043,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        g.save()

        km = 20
        nWorkingDay = 2
        for status in ["researcher", "researcher", "engineer", "student"]:
            s = SurveyAnswer(
                ghgi=g,
                nWorkingDay=nWorkingDay,
                position=status,
                nWorkingDay2=nWorkingDay / 2,
            )
            s.save()
            cs1 = CommuteSection(
                survey=s,
                mode="car",
                distance=km,
                isDay2=False,
                pooling=False,
                engine="diesel",
            )
            cs1.save()
            cs2 = CommuteSection(
                survey=s, mode="bus", distance=10, isDay2=True, pooling=False
            )
            cs2.save()

        # create a ghgi
        g = GHGI(
            laboratory=l,
            year=2044,
            nResearcher=2,
            nProfessor=2,
            nEngineer=2,
            nStudent=2,
            budget=int(10e6),
        )
        g.save()

        for status in ["researcher", "researcher", "engineer", "student"]:
            s = SurveyAnswer(ghgi=g, nWorkingDay=random.randint(1, 5), position=status)
            cs = CommuteSection(
                survey=s,
                mode="car",
                engine="diesel",
                distance=random.randint(1, 30),
                isDay2=False,
            )
            s.save()
            cs.save()


def create_users_labs():
    """Create some users and lab"""

    # this create 2 * 5 + 1 users
    # 1 admin
    # 5 users with a lab
    # 5 users without a lab
    admins, users, users_wo_labs = create_test_users(users_nb=5)

    # create some labs with disciplines, administrations
    all_disciplines_data = [
        dict(id=d.id, percentage=50) for d in Discipline.objects.all()
    ]
    all_administrations_data = Administration.objects.all()
    for i_user, u in enumerate(users):
        # create some public actions
        laboratory = u.laboratory.first()
        # add 2 disciplines + 2 administrations
        disciplines_data = [
            all_disciplines_data[(i_user) % len(all_disciplines_data)]
        ] + [all_disciplines_data[(i_user + 1) % len(all_disciplines_data)]]
        for discipline_data in disciplines_data:
            d, created = LaboratoryDiscipline.objects.update_or_create(
                discipline_id=discipline_data["id"],
                laboratory_id=laboratory.id,
                percentage=discipline_data["percentage"],
            )
        administrations_data = [
            all_administrations_data[(i_user) % len(all_administrations_data)]
        ] + [all_administrations_data[(i_user + 1) % len(all_administrations_data)]]
        laboratory.administrations.set(administrations_data)
    return admins, users, users_wo_labs


class Command(BaseCommand):
    """Populate the Database with some data.

    => TESTING Purpose only
    """

    def add_arguments(self, parser):
        parser.add_argument(
            "args",
            metavar="app_label",
            nargs="+",
            help="One or more application label.",
        )

    def handle(self, *app_labels, **options):
        from django.apps import apps

        try:
            app_configs = [apps.get_app_config(app_label) for app_label in app_labels]
        except (LookupError, ImportError) as e:
            raise CommandError(
                "%s. Are you sure your INSTALLED_APPS setting is correct?" % e
            )

        # first populate the DB with some users / admin / labs
        # create some tests and assign them with some initiatives
        _, users, _ = create_test_users(users_nb=5)

        for app_config in app_configs:
            self.handle_app_config(users, app_config, **options)

    def handle_app_config(
        self, users: List[L1P5User], app_config: Any, **options: Any
    ) -> None:
        import sys

        populate_fnc = getattr(
            sys.modules[__name__], f"populate_{app_config.label}", None
        )
        if not populate_fnc:
            return
        populate_fnc(users)
