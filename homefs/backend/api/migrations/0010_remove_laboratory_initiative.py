# Generated by Django 3.2.7 on 2022-10-18 06:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_rename_laboratoire_laboratory'),
        ('transition', '0005_update_initiative'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='laboratory',
            name='initiative',
        ),
    ]
