# utils fonction to populate the db with some predefined resources
from contextlib import contextmanager
import random
from typing import List, Tuple

from backend.carbon.models import GHGI
from backend.core.models import Laboratory
from backend.users.models import L1P5User


L1P5_USER_EMAIL = "l1p5-test@l1p5.org"
L1P5_USER_PASSWORD = "l1p5-test"
L1P5_LABORATORY = "lab-test"
L1P5_LABORATORY_LNG = 42.0
L1P5_LABORATORY_LAT = 42.0


@contextmanager
def test_user_ctx():
    """Decorator that yields a fresh test user (deletes the previously created one)."""
    email = "l1p5-test@l1p5.org"
    # hardcoded :(
    password = "l1p5-test"

    # delete the user and all the associated resource in cascade
    # lab, initiatives...
    delete_user(email)
    u = ensure_user_created(email)
    yield u, email, password


def with_test_user(f):
    """Inject a fresh user (and email, and password) in the function parameters."""

    def wrapped(*args, **kwargs):
        with test_user_ctx() as (u, email, password):
            return f(u, email, password, *args, **kwargs)

    return wrapped


def delete_user(email: str):
    """Delete all the users(s) associated with the email."""
    users = L1P5User.objects.filter(email=email)
    for user in users:
        user.delete()


def ensure_user_created(email: str = L1P5_USER_EMAIL) -> L1P5User:
    """Ensure that the test user is created.

    Idempotent
    """
    u, _ = L1P5User.objects.get_or_create(email=email, defaults=dict(is_active=True))
    u.set_password(L1P5_USER_PASSWORD)
    u.save()
    return u


def ensure_admin_created(email: str = L1P5_USER_EMAIL) -> L1P5User:
    """Ensure to create a super user

    Idempotent
    """
    u = ensure_user_created(email=email)
    u.is_staff = True
    u.is_superuser = True
    u.save()
    return u


def ensure_lab_created(
    referent: L1P5User,
    name: str = L1P5_LABORATORY,
    latitude=L1P5_LABORATORY_LAT,
    longitude=L1P5_LABORATORY_LNG,
    **kwargs,
) -> Laboratory:
    """Ensure a lab is created.

    Idempotent
    """
    l, _ = Laboratory.objects.get_or_create(
        name=name,
        defaults=dict(
            referent=referent, latitude=latitude, longitude=longitude, **kwargs
        ),
    )
    return l


def create_test_users(users_nb: int = 5) -> Tuple[List[L1P5User], List[L1P5User], List[L1P5User]]:
    """Creates a bunch of test users.

    l1p5-test-admin@l1p5.org: get superuser permissions
    l1p5-test-lab-XXX@l1p5.org: standard user with a lab associated and an initial GHGI
    l1p5-test-nolab-XXX@l1p5.org: standard user with no lab associated

    All of these users have the same password: l1p5-test
    """
    admins, users, users_without_labs = [], [], []
    email = "l1p5-test-admin@l1p5.org"
    delete_user(email)
    u = ensure_admin_created(email)
    admins.append(u)
    for i in range(users_nb):
        email = f"l1p5-test-lab-{i}@l1p5.org"
        delete_user(email)
        u = ensure_user_created(email)
        # make it a referent for a laboratory
        l = ensure_lab_created(
            u,
            name=f"LAB_OF_{email}",
            latitude=random.randint(-90, 90),
            longitude=random.randint(-180, 180),
        )

        users.append(u)

    for i in range(5):
        email2 = f"l1p5-test-nolab-{i}@l1p5.org"
        delete_user(email2)
        u = ensure_user_created(email2)
        users_without_labs.append(u)

    return admins, users, users_without_labs
