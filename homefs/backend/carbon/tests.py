from typing import Tuple

from django.test import Client
from django.test import TestCase
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status

from backend.carbon.models import GHGI, ALL_MODULES, SurveyAnswer, CommuteSection
from backend.users.models import L1P5User
from backend.test_utils import ensure_user_created, ensure_lab_created


REQUEST_KWARGS = dict(content_type="application/json")


def test_intensity(i: int = 0):
    """util fonction that builds an intensity."""
    return {
        "intensity": i,
        "uncertainty": i + 1,
    }


def make_submitted():
    return {m: False for m in ALL_MODULES}


def make_synthesis(synthesis=None):
    if synthesis is None:
        synthesis = {}
    s = {m: test_intensity(-42) for m in ALL_MODULES}
    s.update(synthesis)
    return s


def make_client(email: str) -> Tuple[L1P5User, Client]:
    user = ensure_user_created(email)
    refresh = RefreshToken.for_user(user)
    # in django >= 4.2 we could use the headers for that
    client = Client(
        HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
    )
    return user, client


class TestGetGHGI(TestCase):
    def setUp(self) -> None:
        self.unauthenticated = Client()
        # create a user with a ghgi
        self.user, self.client = make_client("test@l1p5.org")

        # create also an admin
        self.admin, self.admin_client = make_client("admin@l1p5.org")
        self.admin.is_superuser = True
        self.admin.save()

        self.laboratory = ensure_lab_created(self.user)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

    def test_get_all_ghgi_without_consumptions(self):
        r = self.unauthenticated.get(
            "/api/get_all_ghgi_without_consumptions/", **REQUEST_KWARGS
        )
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            r.status_code,
            "unauthenticated user can't get any ghgi",
        )

        r = self.client.get("/api/get_all_ghgi_without_consumptions/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_200_OK, r.status_code, "authenticated user can get their ghgis"
        )

    def test_get_ghgis_consumptions(self):
        r = self.unauthenticated.get("/api/get_all_ghgi/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            r.status_code,
            "unauthenticated user can't get any ghgi",
        )

        r = self.client.get("/api/get_all_ghgi/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_200_OK, r.status_code, "authenticated user can get their ghgis"
        )

    # admin stuffs
    def test_get_all_ghgi_admin(self):
        r = self.unauthenticated.get("/api/get_all_ghgi_admin/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            r.status_code,
            "unauthenticated user can't get any ghgi",
        )

        r = self.client.post("/api/get_all_ghgi_admin/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_204_NO_CONTENT,
            r.status_code,
            "non admin can't access all the ghgis",
        )

        r = self.admin_client.post("/api/get_all_ghgi_admin/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_200_OK, r.status_code, "admin can access all the ghgis"
        )
        self.assertEqual(1, len(r.json()))

    def test_get_ghgis_consumption(self):
        r = self.unauthenticated.post("/api/get_ghgis_consumptions/", **REQUEST_KWARGS)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            r.status_code,
            "unauthenticated user can't get any ghgi",
        )

        r = self.client.post(
            "/api/get_ghgis_consumptions/",
            dict(module="buildings", ghgis=[self.ghgi.id]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_204_NO_CONTENT,
            r.status_code,
            "non admin can't access all the ghgis",
        )

        r = self.admin_client.post(
            "/api/get_ghgis_consumptions/",
            dict(module="buildings", ghgis=[self.ghgi.id]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            status.HTTP_200_OK, r.status_code, "admin can access all the ghgis"
        )
        self.assertEqual(1, len(r.json()))


class TestPermissions(TestCase):
    def setUp(self) -> None:
        self.api_unauthenticated = Client()
        # create a user with a ghgi
        self.victim = ensure_user_created("victim@l1p5.org")
        refresh = RefreshToken.for_user(self.victim)
        # in django >= 4.2 we could use the headers for that
        self.api_victim = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

        self.laboratory = ensure_lab_created(self.victim)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        # create a second user (attacker)
        self.attacker = ensure_user_created(email="attacker@l1p5.org")
        refresh = RefreshToken.for_user(self.attacker)
        # in django >= 4.2 we could use the headers for that
        self.api_attacker = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def _assert(
        self,
        client: Client,
        *,
        url: str,
        data: dict,
        expected_status: int,
        message: str,
    ):
        r = client.post(url, data, **REQUEST_KWARGS)
        self.assertEqual(expected_status, r.status_code, message)

    def _assert_permissions(
        self, *, url: str, data: dict, expected_status=status.HTTP_200_OK
    ):
        self._assert(
            self.api_unauthenticated,
            url=url,
            data=data,
            expected_status=status.HTTP_401_UNAUTHORIZED,
            message=f"Unauthenticated user can't access {url}",
        )

        self._assert(
            self.api_attacker,
            url=url,
            data=data,
            expected_status=status.HTTP_403_FORBIDDEN,
            message="Another user can't act on {url}",
        )
        # but victim can
        self._assert(
            self.api_victim,
            url=url,
            data=data,
            expected_status=expected_status,
            message=f"A user can use {url}",
        )

    def test_get_someonelse_s_ghgi_consumptions(self):
        self._assert_permissions(
            url="/api/get_ghgi_consumptions/",
            data=dict(ghgi_id=self.ghgi.id),
        )

    def test_save_someoneelse_s_ghgi(self):
        self._assert_permissions(
            url="/api/save_ghgi/",
            data=dict(id=self.ghgi.id, submitted=make_submitted()),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_save_someoneelse_s_vehicles(self):
        self._assert_permissions(
            url="/api/save_vehicles/",
            data=dict(ghgi_id=self.ghgi.id, vehicles=[], synthesis=make_synthesis()),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_save_someoneelse_s_devices(self):
        self._assert_permissions(
            url="/api/save_computer_devices/",
            data=dict(ghgi_id=self.ghgi.id, devices=[], synthesis=make_synthesis()),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_save_someoneelse_s_travels(self):
        self._assert_permissions(
            url="/api/save_travels/",
            data=dict(ghgi_id=self.ghgi.id, travels=[], synthesis=make_synthesis()),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_save_someoneelse_s_purchases(self):
        # attacker can't save victim ghgi
        self._assert_permissions(
            url="/api/save_purchases/",
            data=dict(ghgi_id=self.ghgi.id, purchases=[], synthesis=make_synthesis()),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_save_someoneelse_s_survey_message(self):
        # attacker can't save victim ghgi
        self._assert_permissions(
            url="/api/save_survey_message/",
            data=dict(ghgi_id=self.ghgi.id, surveyMessage="test"),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_clone_someoneelse_s_survey_message(self):
        self._assert_permissions(
            url="/api/clone_survey/",
            data=dict(ghgi_id=self.ghgi.id, surveyCloneYear="2042"),
            expected_status=status.HTTP_201_CREATED,
        )

    def test_delete_someoneelse_s_ghgi(self):
        self._assert_permissions(
            url="/api/delete_ghgi/",
            data=dict(ghgi_id=self.ghgi.id),
            expected_status=status.HTTP_204_NO_CONTENT,
        )

    def test_submit_someoneelse_s_ghgi(self):
        # attacker can't save victim ghgi
        submitted = make_submitted()
        submitted["heatings"] = True

        self._assert_permissions(
            url="/api/update_submitted/",
            data=dict(ghgi_id=self.ghgi.id, submitted=submitted),
            expected_status=status.HTTP_204_NO_CONTENT,
        )


def test_intensity(i: int = 0):
    """util fonction that builds an intensity."""
    return {
        "intensity": i,
        "uncertainty": i + 1,
    }


class SaveSynthesis(TestCase):
    def setUp(self) -> None:
        # create some resources
        self.user = ensure_user_created()
        self.laboratory = ensure_lab_created(self.user)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        # create a user to interact with the view
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api_client = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def generic_save_module(self, module, endpoint):
        """Save a simple synthesis and check its persistence.

        This injects a custom (not valid) synthesis and check it's been
        recorded as it is.
        """
        data = {
            "ghgi_id": self.ghgi.id,
            module: [],
            "synthesis": make_synthesis({module: test_intensity(0)}),
        }

        r = self.api_client.post(endpoint, data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(test_intensity(0), actual_synthesis[module])

    def test_save_travels(self):
        self.generic_save_module("travels", "/api/save_travels/")

    def test_save_commutes(self):
        self.generic_save_module("commutes", "/api/save_commutes/")

    def test_save_vehicles(self):
        self.generic_save_module("vehicles", "/api/save_vehicles/")

    def test_save_computer_devices(self):
        self.generic_save_module("devices", "/api/save_computer_devices/")

    def test_save_purchase(self):
        self.generic_save_module("purchases", "/api/save_purchases/")

    def test_save_buildings(self):
        synthesis = make_synthesis(
            dict(
                heatings=test_intensity(0),
                electricity=test_intensity(1),
                refrigerants=test_intensity(2),
            )
        )
        data = dict(ghgi_id=self.ghgi.id, buildings=[], synthesis=synthesis)

        r = self.api_client.post("/api/save_buildings/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(synthesis, actual_synthesis)

    def test_save_activate_survey(self):
        synthesis = make_synthesis()
        synthesis["travels"] = test_intensity(3)

        data = dict(
            ghgi_id=self.ghgi.id,
            commutesActive=self.ghgi.commutesActive,
            foodsActive=self.ghgi.foodsActive,
            synthesis=synthesis
        )
        r = self.api_client.post("/api/update_survey_active/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_204_NO_CONTENT, r.status_code)

        # get the ghgi to check if the synthesis is stored correctly
        ghgi_submitted = GHGI.objects.get(id=self.ghgi.id)

        actual_synthesis = ghgi_submitted.synthesis
        self.assertDictEqual(synthesis, actual_synthesis)

    def test_save_wrong_format(self):
        synthesis = make_synthesis()
        synthesis["extra_field"] = test_intensity(0)

        data = dict(ghgi_id=self.ghgi.id, buildings=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_buildings/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, commutes=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_commutes/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, devices=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_computer_devices/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, purchases=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_purchases/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, travels=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_travels/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        data = dict(ghgi_id=self.ghgi.id, vehicles=[], synthesis=synthesis)
        r = self.api_client.post("/api/save_vehicles/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

        # this also runs on activate survey
        data = dict(
            ghgi_id=self.ghgi.id,
            commutesActive=self.ghgi.commutesActive,
            foodsActive=self.ghgi.foodsActive,
            synthesis=synthesis
        )
        r = self.api_client.post("/api/update_survey_active/", data, **REQUEST_KWARGS)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)

    def tearDown(self) -> None:
        self.user.delete()


class TestCloneSurvey(TestCase):
    def setUp(self) -> None:
        # create some resources
        self.user = ensure_user_created()
        self.laboratory = ensure_lab_created(self.user)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        # associate a section
        c = SurveyAnswer(ghgi=self.ghgi, nWorkingDay=1, position="researcher")
        cs = CommuteSection(survey=c, mode="car", engine="diesel", distance=10)
        c.save()
        cs.save()

        # create a user to interact with the view
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api_client = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_clone(self):
        # create a new ghgi
        new_ghgi = GHGI(
            laboratory=self.laboratory,
            year=2043,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        new_ghgi.save()

        # clone previous year
        r = self.api_client.post(
            "/api/clone_survey/",
            dict(ghgi_id=new_ghgi.id, surveyCloneYear=2042),
            **REQUEST_KWARGS,
        )
        self.assertEqual(1, len(r.json()), "Should contain 1 commute after clone")
        self.assertEqual(
            1, len(r.json()[0]["sections"]), "Should contain 1 commute section"
        )
        # at this point we only return the cloned commutes
        # it's up to the user to save them with the new ghgi
