from django.core.management.base import BaseCommand

from backend.users.models import Role
from backend.utils import make


class Command(BaseCommand):
    help = """Applying a permission profile to a user"""

    def add_arguments(self, parser):
        parser.add_argument("role", choices=[r.value for r in Role])
        parser.add_argument("email", type=str)

    def handle(self, *args, **options):
        role = options["role"]
        email = options["email"]

        make(role, email=email)
