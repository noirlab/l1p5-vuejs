import { test, expect } from '@playwright/test'
import {
  anonymousConnection,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadVehicles
} from './utils'

import { join } from 'path'

test('[anonymous] upload building template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadBuildings({ page })
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload devices template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadDevices({ page })

  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload purchases template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadPurchases({ page })
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload vechicles template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadVehicles({ page })
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})
