#!/usr/bin/env bash

set -eu

DIR=$(dirname "${BASH_SOURCE[0]}")


function usage {
        echo "Usage: $(basename $0) [-n]" 2>&1
        echo "   -n   do not recreate the playwright container, use it"
        echo "   -u   update screenshots"
        exit 1
}

optstring="nu"

CREATE=1
UPDATE=0
while getopts ${optstring} arg; do
  case "${arg}" in
    n)
		CREATE=0
		;;
	u)
		UPDATE=1
		;;
    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      usage
      ;;
  esac
done

shift $(( OPTIND - 1 ))


#
# Prepare snapshots to be used on the CI (linux machine)
# 

PREFIX_EXEC () {
	echo "docker exec $1 playwright"	
}

if [ $CREATE -eq 1 ]
then
    docker run -d \
        --name playwright \
        -v $DIR/../../:/work/ \
        debian:12 \
        sleep infinity
	$(PREFIX_EXEC "") apt update
	$(PREFIX_EXEC "") apt install -y git procps rsync python3 python3-pip pkg-config libmariadb-dev
  $(PREFIX_EXEC "") git clone --depth 1 /work /tmp/work
 	$(PREFIX_EXEC "-w /tmp/work") rootfs/opt/l1p5/bin/install-node.sh
	$(PREFIX_EXEC "-w /tmp/work") pip install --break-system-packages -r rootfs/opt/l1p5/requirements.txt
fi

# update local working copy
$(PREFIX_EXEC "-w /tmp/work") git pull

if [ $UPDATE -eq 1 ]
then
  # run the test
  # see playwright.config.js to see the effect of setting CI=1
  $(PREFIX_EXEC "-w /tmp/work/homefs -e CI=1" ) ./tests/run-functional.sh $@ --update-snapshots || true

  # get back the golden images
  # FIXME make it generic (but we can't use shell expansion like *snapshots since
  # it's expanded outside the containeru
  $(PREFIX_EXEC "") rsync -avz /tmp/work/homefs/tests/unauthenticated.spec.js-snapshots /work/homefs/tests/. || true
  $(PREFIX_EXEC "") rsync -avz /tmp/work/homefs/tests/authenticated.spec.js-snapshots /work/homefs/tests/. || true
else
  $(PREFIX_EXEC "-w /tmp/work/homefs -e CI=1" ) ./tests/run-functional.sh $@ || true
fi

# also copy test-result
$(PREFIX_EXEC "-w /tmp/work/homefs") rsync -avz test-results /work/homefs || true
$(PREFIX_EXEC "-w /tmp/work/homefs") rsync -avz playwright-report /work/homefs || true