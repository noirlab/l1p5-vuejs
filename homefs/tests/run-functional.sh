#!/usr/bin/env bash

# Used in CI to run the tests

set -ux

npm install --include-dev

npx playwright install --with-deps

# make sure we kill anyway
npx playwright test $@
rc=$?
sleep 1

echo "Exiting"
exit $rc
