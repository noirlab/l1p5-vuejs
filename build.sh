#!/bin/bash

# Build source code for l1p5
# Used for development purpose

# @author  Fabrice Jammes

set -euxo pipefail

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Build source code for l1p5
Used for development purpose

EOD
}

# Get the options
while getopts h c ; do
    case $c in
        h) usage ; exit 0 ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

WORKDIR="/home/labo15-au1"
echo "Build l1p5 website source code"
docker exec -it l1p5 "$WORKDIR"/build.sh

